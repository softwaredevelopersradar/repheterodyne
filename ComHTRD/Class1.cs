﻿using System;
using System.Linq;
using SDS;
using System.Runtime.InteropServices;
using System.IO.Ports;
using System.Threading;

namespace CNT
{

    [StructLayout(LayoutKind.Sequential, Pack = 1)] // size = 3
   public struct MainFiel
    {
        public byte Address;
        public byte cipher;
        public byte FieldLength;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct LastPartAnswer
    {
        public byte ErrorCode;
        public ushort CRC;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SwitchingPartAnswer
    {
        public byte SwitchingFreq;
        public ushort CRC;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SpecialPartAnswer
    {
        public byte T;
        public byte H;
        public byte Charge;
        public ushort CRC;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct RequestPPRCH
    {

        public byte Step;

        public byte NumberFreq;

        public Int16 SeltusFreq;

        public byte Duration;

    }
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct RequestFielFreq
    {
        public byte NumberFreq;

        public byte Step;

        public byte Duration;

    }

    [StructLayout(LayoutKind.Explicit,  Size = 7)]
    public struct Request
    {
        [FieldOffset(0)]
        public MainFiel mainFiel;
        [FieldOffset(3)]
        public Int16 Frequency;
        [FieldOffset(6)]
        public byte Duration;
    }

    public struct Answer
    {
        public MainFiel mainFiel;
        public object obj;
    }

    public enum FrequenceForSwitching : byte
    {
        _434 = 0x00,
        _868 = 0x01
    }

    public class ComHTRD
    {
        private const byte LEN_HEAD = 3;
        
        
        private SerialPort _port;
        private Thread thrRead;
        const byte AddressToGen = 0x12;
        private const byte AddressToComp = 0x21;
        public const byte OffSignal = 0x10;
        public const byte FRCH = 0x21;
        public const byte AM = 0x31;
        public const byte FM = 0x41;
        public const byte CHM = 0x51;
        public const byte FielFreq = 0x61;
        public const byte PPRCH = 0x71;
        public const byte Answer = 0xAA;
        public const byte SpecialCipher = 0xF1;
        public const byte ChangeFreq = 0xE1;
        public byte[] ListCodegram = { FRCH, AM, FM, CHM };
        public byte[] listError = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x0D, 0x10, 0x11 };
        //private byte RequestCipher = 0;
        
        #region Delegates

        public delegate void ConnectEventHandler();
        public delegate void ByteEventHandler(byte[] bByte);
        public delegate void CmdEventHandler(object obj);
        //public delegate void ReceiveEventHandler(object obj, byte code);



        #endregion

        #region Events

       
        public event ConnectEventHandler OnConnectPort;
        public event ConnectEventHandler OnDisconnectPort;

        public event ByteEventHandler OnReadByte;
        public event ByteEventHandler OnWriteByte;


        public event CmdEventHandler OnReceiveCmd;
        public event CmdEventHandler OnSendCmd;
        //public event CmdEventHandler OnReceive_ERROR;
        #endregion

        private byte[] SerializationFreq(Int32 frequency)
        {
            byte[] sendPPRCH = new byte[3];
            sendPPRCH[0] = (byte) (frequency >> 16);
            sendPPRCH[1] = (byte) (frequency >> 8);
            sendPPRCH[2] = (byte) frequency;
            return sendPPRCH;
        }

        object FindCmd(ref byte[] buffer, ref int index )
        {
            if (index >= 6)
            {
                MainFiel mainFiel = new MainFiel();
                mainFiel.Address = buffer[0];
                mainFiel.cipher = buffer[1];
                mainFiel.FieldLength = buffer[2];
                if (mainFiel.Address != AddressToComp)
                {
                    DellArray(ref buffer, 1);
                    index--;
                    return null;
                }
                else
                {
                    switch (mainFiel.cipher)
                    {
                        case OffSignal: case FRCH: case AM: case FM: case CHM: case FielFreq: case PPRCH:
                            if (index >= 6)
                            {
                                object answer1 = new LastPartAnswer();
                                byte[] bufAnswer = new byte[mainFiel.FieldLength];
                                Array.Copy(buffer, 3, bufAnswer, 0, mainFiel.FieldLength);
                                SdS.Deserialization(bufAnswer, ref answer1);
                                Answer comAnswer = new Answer();
                                comAnswer.mainFiel = mainFiel;
                                comAnswer.obj = answer1;
                               DellArray(ref buffer, 6);
                                index  -= 6;
                                return comAnswer;//6 byte
                            }
                            break;
                        case SpecialCipher:
                            if (index >= 8)
                            {
                                object answer1 = new SpecialPartAnswer();
                                byte[] bufAnswer = new byte[mainFiel.FieldLength];
                                Array.Copy(buffer, 3, bufAnswer, 0, mainFiel.FieldLength);
                                SdS.Deserialization(bufAnswer, ref answer1);
                                Answer comAnswer = new Answer();
                                comAnswer.mainFiel = mainFiel;
                                comAnswer.obj = answer1;
                                DellArray(ref buffer, 8);
                                index -= 8;
                                return comAnswer;//8 byte
                            }
                            break;
                        case ChangeFreq:
                            if (index >= 6)
                            {
                                object answer1 = new SwitchingPartAnswer();
                                byte[] bufAnswer = new byte[mainFiel.FieldLength];
                                Array.Copy(buffer, 3, bufAnswer, 0, mainFiel.FieldLength);
                                SdS.Deserialization(bufAnswer, ref answer1);
                                Answer comAnswer = new Answer();
                                comAnswer.mainFiel = mainFiel;
                                comAnswer.obj = answer1;
                                DellArray(ref buffer, 6);
                                index -= 6;
                                return comAnswer;//8 byte
                            }
                            break;
                        default:
                            DellArray(ref buffer, 2);
                            index -= 2;
                            return null;
                            //break;
                    }
                }
            }
            return null;
        }

        public void DellArray(ref byte[] arrayBytes, int length)
        {

            Array.Clear(arrayBytes, 0, length);
            arrayBytes.Reverse();
            Array.Resize(ref arrayBytes, (arrayBytes.Length - length));
            arrayBytes.Reverse();
            
        }

        public string FindErr(byte error)
        {
            string messError = "";
            switch (error)
            {
                case 0x00:
                    messError = "Нет ошибки";
                    return messError;
                    //break;
                case 0x01:
                    messError = "Ошибка программная неопределенная";
                    break;
                case 0x02:
                    messError = "Нет ответа от LoRa";
                    break;
                case 0x03:
                    messError = "Нет ответа от другого передатчика по каналу LoRa (250мс) ";
                    break;
                case 0x04:
                    messError = "Ошибка передачи; сделать повторный Init";
                    break;
                case 0x05:
                    messError = "Нет синхронизации";
                    break;
                case 0x06:
                    messError = "Ошибка ввода данных";
                    break;
                case 0x0D:
                    messError = "Нет связи с Формирователем";
                    break;
                case 0x10:
                    messError = "ПР - на ЖК при приеме с ПК (внутренние для ЖК)";
                    break;
                case 0x11:
                    messError = "ПРД - на ЖК при передаче на ПК (внутренние для ЖК)";
                    break;
            }
            return messError;
        }


        public void OpenPort(string portName, Int32 baudRate, System.IO.Ports.Parity parity, Int32 dataBits, System.IO.Ports.StopBits stopBits)
        {
            // Open COM port

            if (_port == null)
                _port = new SerialPort();

            // if port is open
            if (_port.IsOpen)

                // close it
                ClosePort();

            // try to open 
            try
            {
                // set parameters of port
                _port.PortName = portName;
                _port.BaudRate = baudRate;

                _port.Parity = parity;
                _port.DataBits = dataBits;
                _port.StopBits = stopBits;

                _port.RtsEnable = true;
                _port.DtrEnable = true;

                //_port.ReadTimeout = 1000;
                //_port.WriteTimeout = 1000;

                _port.ReceivedBytesThreshold = 1000;
                // set parameters of port
                /*  _port.PortName = portName;
                  _port.BaudRate = baudRate;

                  _port.Parity = System.IO.Ports.Parity.None;
                  _port.DataBits = 8;
                  _port.StopBits = System.IO.Ports.StopBits.One;*/


                // open it
                _port.Open();

                // create the thread for reading data from the port
                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }

                // load function of the thread for reading data from the port
                try
                {
                    thrRead = new Thread(new ThreadStart(ReadData));
                    thrRead.IsBackground = true;
                    thrRead.Start();
                }
                catch (System.Exception)
                {

                }

                // raise event
                // ConnectPort();

                // return true;
                if (OnConnectPort != null)
                    OnConnectPort();
            }
            catch (System.Exception)
            {
                if (OnDisconnectPort != null)
                    OnDisconnectPort();

                //return false;
            }
        }

        public bool PortConnect()
        {
            try
            {

                if (_port.IsOpen)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        // Close COM port
        public void ClosePort()
        {
            // clear in buffer
           try
            {
                _port.DiscardInBuffer();
            }
            catch (System.Exception ex)
            {
                ex.GetBaseException();
            }

            // clear in buffer
            try
            {
                _port.DiscardOutBuffer();
            }
            catch (System.Exception ex)
            {
                ex.GetBaseException();
            }

            try
            {
                // close port
                _port.Close();

                // destroy thread of reading
                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }

                if (OnDisconnectPort != null)
                    // raise event
                    OnDisconnectPort();
            }

            catch (System.Exception ex)
            {
                ex.GetBaseException();
            }
        }


        // Read data (byte) from port (thread)
        private void ReadData()
        {
            // buffer for reading bytes
            byte[] bBufRead;
            bBufRead = new byte[256];

            // buffer to transfer and save reading bytes
            byte[] bBufSaveS = null;
            //bBufSaveS = new byte[200];

            // length of reading bytes
            int iReadByte = -1;
            int iBufSave = 0;

            // while port open
            while (true)
            {
                try
                {
                    bBufRead = new byte[256];

                    // clear burre of reading
                    Array.Clear(bBufRead, 0, bBufRead.Length);

                    iReadByte = 0;

                    // read data
                    iReadByte = _port.Read(bBufRead, 0, bBufRead.Length);

                    // if bytes was read
                    if (iReadByte > 0)
                    {
                        Array.Resize(ref bBufRead, iReadByte);

                        if (OnReadByte != null)
                            OnReadByte(bBufRead);

                        if (bBufSaveS == null)
                            bBufSaveS = new byte[iReadByte];
                        else
                            Array.Resize(ref bBufSaveS, bBufSaveS.Length + iReadByte);


                        Array.Copy(bBufRead, 0, bBufSaveS, bBufSaveS.Length - iReadByte, iReadByte);
                        iBufSave += iReadByte;                                     
                        try
                        {
                            while (iBufSave >= 6)
                            {
                                MainFiel mainFiel = new MainFiel();
                                mainFiel.Address = bBufSaveS[0];
                                mainFiel.cipher = bBufSaveS[1];
                                mainFiel.FieldLength = bBufSaveS[2];

                                object answers = null;
                                Answer answer = new Answer();

                                if (iBufSave >= LEN_HEAD + mainFiel.FieldLength)
                                {
                                    byte[] bData = new byte[LEN_HEAD + mainFiel.FieldLength];

                                    Array.Copy(bBufSaveS, 0, bData, 0, LEN_HEAD + mainFiel.FieldLength);
                                    int iLen = LEN_HEAD + mainFiel.FieldLength;
                                    answers = FindCmd(ref bData, ref iLen);

                                    Array.Reverse(bBufSaveS);
                                    //Array.Resize(ref bBufSaveS, bBufSaveS.Length - (LEN_HEAD + mainFiel.FieldLength - iLen));
                                    Array.Resize(ref bBufSaveS, bBufSaveS.Length - (LEN_HEAD + mainFiel.FieldLength));
                                    Array.Reverse(bBufSaveS);

                                    //iBufSave -= (LEN_HEAD + mainFiel.FieldLength - iLen);

                                    iBufSave -= (LEN_HEAD + mainFiel.FieldLength);

                                    if (answers != null)
                                        answer = (Answer)answers;


                                    if ((object)answer != null)
                                    {
                                        if (OnReceiveCmd != null)
                                        {
                                            OnReceiveCmd((object)answer);
                                        }

                                    }

                                }
                                else
                                {
                                    Array.Reverse(bBufSaveS);
                                    //Array.Resize(ref bBufSaveS, bBufSaveS.Length - (LEN_HEAD + mainFiel.FieldLength - iLen));
                                    Array.Resize(ref bBufSaveS, 0);
                                    Array.Reverse(bBufSaveS);

                                    //iBufSave -= (LEN_HEAD + mainFiel.FieldLength - iLen);

                                    iBufSave = 0;
                                }
                            }

                        }
                        catch (Exception exeption)
                        {
                            string mess = exeption.Message;
                            Console.Write(mess);
                        }
                        

                    }

                    else
                        if(OnDisconnectPort != null)
                            OnDisconnectPort();
                }

                catch (System.Exception)
                {

                    ClosePort();

                }

                Thread.Sleep(5);
            }
        }

        public bool Send(Int32 frequency, byte Code, byte numberFreq, byte step, Int16 saltusFreq, byte time) // length information  5 byte
        {
            int i = 0;
            byte[] freq = SerializationFreq(frequency);
            //RequestCipher = Code;
            
            switch (Code)
            {
                case FRCH:case AM:case FM:case CHM:
                    MainFiel mainFiel;
                    mainFiel.Address = AddressToGen;
                    mainFiel.cipher = Code;
                    mainFiel.FieldLength = 6; // было 5
                    byte[] send = SdS.Serialization(mainFiel);
                    i = send.Length;
                    Array.Resize(ref send, send.Length + freq.Length);
                    Array.Copy(freq, 0, send, i, freq.Length);
                    i = send.Length;
                    Array.Resize(ref send, send.Length + 1);
                    send[i] = time;
                    byte[] crc = CRC16(send, (ushort) (mainFiel.FieldLength + 1));
                    i = send.Length;
                    Array.Resize(ref send, send.Length + crc.Length);
                    send[i++] = crc[1];
                    send[i] = crc[0];
                    return WriteData(send);
                    //break;
                case FielFreq:
                    RequestFielFreq requestFileFreq;
                    MainFiel mainFielFreq;
                    mainFielFreq.Address = AddressToGen;
                    mainFielFreq.cipher = Code;
                    mainFielFreq.FieldLength = 8;//было 7
                    byte[] sendFielFreq = SdS.Serialization(mainFielFreq);
                    i = sendFielFreq.Length;
                    Array.Resize(ref sendFielFreq, sendFielFreq.Length + freq.Length);
                    Array.Copy(freq, 0, sendFielFreq, i, freq.Length);
                    requestFileFreq.NumberFreq = numberFreq;
                    requestFileFreq.Step = step;
                    requestFileFreq.Duration = time;
                    byte[] PartFileFreq = SdS.Serialization(requestFileFreq);
                    i = sendFielFreq.Length;
                    Array.Resize(ref sendFielFreq, sendFielFreq.Length + PartFileFreq.Length);
                    Array.Copy(PartFileFreq, 0, sendFielFreq, i, PartFileFreq.Length);
                    byte[] crcFielFreq = CRC16(sendFielFreq, (ushort)(mainFielFreq.FieldLength + 1));
                    i = sendFielFreq.Length;
                    Array.Resize(ref sendFielFreq, sendFielFreq.Length + crcFielFreq.Length);
                    sendFielFreq[i++] = crcFielFreq[1];
                    sendFielFreq[i] = crcFielFreq[0];
                    return WriteData(sendFielFreq);
                case PPRCH:
                    RequestPPRCH requestPPRCH;
                    MainFiel mainFielPPRCH;
                    mainFielPPRCH.Address = AddressToGen;
                    mainFielPPRCH.cipher = PPRCH;
                    mainFielPPRCH.FieldLength = 10; // было 9
                    byte[] sendPPRCH = SdS.Serialization(mainFielPPRCH);
                    i = sendPPRCH.Length;
                    Array.Resize(ref sendPPRCH, sendPPRCH.Length + freq.Length);
                    Array.Copy(freq, 0, sendPPRCH, i, freq.Length);
                    requestPPRCH.NumberFreq = numberFreq;
                    requestPPRCH.Step = step;
                    requestPPRCH.SeltusFreq = saltusFreq;
                    requestPPRCH.Duration = time;
                    byte[] Part = SdS.Serialization(requestPPRCH);
                    i = sendPPRCH.Length;
                    Array.Resize(ref sendPPRCH, sendPPRCH.Length + Part.Length);
                    Array.Copy(Part, 0, sendPPRCH, i, Part.Length);
                    byte bFreqsaltus = sendPPRCH[sendPPRCH.Length - 4];                    
                   sendPPRCH[sendPPRCH.Length - 4] = sendPPRCH[sendPPRCH.Length - 5];
                   sendPPRCH[sendPPRCH.Length - 5] = bFreqsaltus;
                    byte[] crcPPRCH = CRC16(sendPPRCH, (ushort)(mainFielPPRCH.FieldLength + 1));
                    i = sendPPRCH.Length;
                    Array.Resize(ref sendPPRCH, sendPPRCH.Length + 2);
                    sendPPRCH[i++] = crcPPRCH[1];
                    sendPPRCH[i] = crcPPRCH[0];
                    return WriteData(sendPPRCH);
                    //break;
                case OffSignal:case SpecialCipher:
                    MainFiel requestSpecial;
                    requestSpecial.Address = AddressToGen;
                    requestSpecial.cipher = Code;
                    requestSpecial.FieldLength = 2;
                    byte[] sendSpecial = SdS.Serialization(requestSpecial);
                    byte[] crcSpecial = CRC16(sendSpecial, (ushort)(requestSpecial.FieldLength + 1));
                    i = sendSpecial.Length;
                    Array.Resize(ref sendSpecial, sendSpecial.Length + crcSpecial.Length);
                    sendSpecial[i++] = crcSpecial[1];
                    sendSpecial[i] = crcSpecial[0];
                    return WriteData(sendSpecial);
                //break;
                default:
                    return false;
                    //break;
            }

        }

        public bool SendFreqSwitchingCommand(FrequenceForSwitching frequency)
        {
            int i = 0;
            MainFiel requestChangeFreq;
            requestChangeFreq.Address = AddressToGen;
            requestChangeFreq.cipher = ChangeFreq;
            requestChangeFreq.FieldLength = 3;
            byte[] sendChangeFreq = SdS.Serialization(requestChangeFreq);
            byte[] crcChangeFreq = CRC16(sendChangeFreq, (ushort)(requestChangeFreq.FieldLength));
            i = sendChangeFreq.Length;
            Array.Resize(ref sendChangeFreq, sendChangeFreq.Length + crcChangeFreq.Length + 1);         
            sendChangeFreq[i] = (byte)frequency;
            sendChangeFreq[i+2] = crcChangeFreq[1];
            sendChangeFreq[i+1] = crcChangeFreq[0];

            return WriteData(sendChangeFreq);
        }


        public void SendAlex()
        {
           
            /*byte[] bSend = new byte[5];
            bSend[0] = 0x12;
            bSend[1] = 0xF1;

            bSend[2] = 0x02;
            bSend[3] = 0x13;
            bSend[4] = 0x90;


           WriteData(bSend);*/
        }

        

        private bool WriteData(byte[] bSend)
        {
            try
            {
                _port.Write(bSend, 0, bSend.Length);

                if (OnWriteByte != null)
                    OnWriteByte(bSend);

                return true;
            }
            catch (System.Exception)
            {
                return false;
            }

        }
        public byte[] CRC16(byte[] Buf, ushort Len)
        {
            const ushort polinom = 0x1021;
            ushort Result = 0xFFFF;

            for (int i = 0; i < Len; ++i)
            {
                Result ^= (ushort)(Buf[i] << 8);
                for (uint j = 0; j < 8; ++j)
                {
                    Result >>= 1;
                    if ((Result & 0x01) != 0) Result ^= polinom;
                }
            }
            byte[] result = BitConverter.GetBytes(Result);
            return result;
        }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct SmallRequest
    {
        public ushort CRC;
    }

    [StructLayout(LayoutKind.Explicit, Size = 5)]
    public struct MiddleRequest
    {
        [FieldOffset(0)]
        public Int16 Frequency;

        [FieldOffset(3)] 
        public ushort CRC;
    }

    [StructLayout(LayoutKind.Explicit, Size = 9)]
    public struct BigRequest
    {
        [FieldOffset(0)]
        public Int16 StartFrequency;

        [FieldOffset(3)] 
        public byte NumberFreq;

        [FieldOffset(4)] 
        public byte Step;

        [FieldOffset(5)] 
        public Int16 FreqSaltus;

        [FieldOffset(7)] 
        public ushort CRC;
    }

    public class ComShaper
    {
        
        private const int LEN_HEAD = 3;

        private SerialPort _port;
        private Thread thrRead;
        public const byte AddressToGen = 0x12;
        public const byte AddressToComp = 0x21;
        public const byte AddressDispToGen = 0x32;
        public const byte AddreddGenToDisp = 0X23;
        public const byte OffSignal = 0x10;
        public const byte FRCH = 0x21;
        public const byte AM = 0x31;
        public const byte FM = 0x41;
        public const byte CHM = 0x51;
        public const byte FielFreq = 0x61;
        public const byte PPRCH = 0x71;
        //public const byte Answer = 0xAA;
        public const byte SpecialCipher = 0xF1;
        public byte[] ListCodegram = { FRCH, AM, FM, CHM};
        public byte[] listError = { 0x00, 0x01, 0x02,  0x03, 0x04, 0x05, 0x06, 0x0D, 0x10, 0x11 };

        #region Delegates

        public delegate void ConnectEventHandler();
        public delegate void ByteEventHandler(byte[] bByte);
        public delegate void CmdEventHandler(object obj);
        public delegate void ReceiveEventHandler(byte[] code);



        #endregion

        #region Events


        public event ComShaper.ConnectEventHandler OnConnectPort;
        public event ComShaper.ConnectEventHandler OnDisconnectPort;

        public event ComShaper.ByteEventHandler OnReadByte;
        public event ComShaper.ByteEventHandler OnWriteByte;


        //public event ComShaper.CmdEventHandler OnReceiveCmd;
        public event ComShaper.ByteEventHandler OnReceiveCmd;
       // public event ComShaper.CmdEventHandler OnSendCmd;
       // public event ComShaper.ReceiveEventHandler OnSendCmd;
        //public event ComShaper.CmdEventHandler OnReceive_ERROR;
        #endregion

        byte[] FindCmd(ref byte[] buffer, ref int index)
        {
            object BufMainFiel = new MainFiel();
            byte[] bufFiel = new byte[3];
            byte[] answerBytes;
            if (index >= 5)
            {
                Array.Copy(buffer, 0, bufFiel, 0, 3);
                SdS.Deserialization(bufFiel, ref BufMainFiel);
                MainFiel mainFiel = (MainFiel)BufMainFiel;
                if (mainFiel.Address != AddressToGen && mainFiel.Address != AddressDispToGen)
                {
                    DellArray(ref buffer, 1);
                    index--;
                    return null;
                }
                else
                {
                    switch (mainFiel.cipher)
                    {
                        case FRCH: case AM: case FM: case CHM:
                            if (index >= 9)
                            {
                                answerBytes = new byte[9];
                                Array.Copy(buffer, 0, answerBytes, 0, answerBytes.Length);
                                DellArray(ref buffer, 9);
                                index -= 9;
                                return answerBytes;
                                /* object answer1 = new MiddleRequest();
                                byte[] bufAnswer = new byte[mainFiel.FieldLength];
                                Array.Copy(buffer, 3, bufAnswer, 0, mainFiel.FieldLength);
                                SdS.Deserialization(bufAnswer, ref answer1);
                                Answer comAnswer = new Answer();
                                comAnswer.mainFiel = mainFiel;
                                comAnswer.obj = answer1;
                                DellArray(ref buffer, 8);
                                index -= 8;
                                return comAnswer;//8 byte
                                */
                            }
                            break;
                        case FielFreq:
                            if (index >= 11)
                            {
                                answerBytes = new byte[11];
                                Array.Copy(buffer, 0, answerBytes, 0, answerBytes.Length);
                                DellArray(ref buffer, 11);
                                index -= 11;
                                return answerBytes;
                                /* object answer1 = new MiddleRequest();
                                byte[] bufAnswer = new byte[mainFiel.FieldLength];
                                Array.Copy(buffer, 3, bufAnswer, 0, mainFiel.FieldLength);
                                SdS.Deserialization(bufAnswer, ref answer1);
                                Answer comAnswer = new Answer();
                                comAnswer.mainFiel = mainFiel;
                                comAnswer.obj = answer1;
                                DellArray(ref buffer, 8);
                                index -= 8;
                                return comAnswer;//8 byte
                                */
                            }
                            break;
                        case PPRCH:
                            if (index >= 13)
                            {

                                answerBytes = new byte[13];
                                Array.Copy(buffer, 0, answerBytes, 0, answerBytes.Length);
                                DellArray(ref buffer, 13);
                                index -= 13;
                                return answerBytes;
                                /*object answer1 = new BigRequest();
                                byte[] bufAnswer = new byte[mainFiel.FieldLength];
                                Array.Copy(buffer, 3, bufAnswer, 0, mainFiel.FieldLength);
                                SdS.Deserialization(bufAnswer, ref answer1);
                                Answer comAnswer = new Answer();
                                comAnswer.mainFiel = mainFiel;
                                comAnswer.obj = answer1;
                                DellArray(ref buffer, 12);
                                index -= 12;
                                return comAnswer;//12 byte
                                */
                            }
                            break;
                        case OffSignal: case SpecialCipher:
                            if(index >= 5)
                            {
                                answerBytes = new byte[5];
                                Array.Copy(buffer, 0, answerBytes, 0, answerBytes.Length);
                                DellArray(ref buffer, 5);
                                index -= 5;
                                return answerBytes;
                                /*object answer1 = new SmallRequest();
                                byte[] bufAnswer = new byte[mainFiel.FieldLength];
                                Array.Copy(buffer, 3, bufAnswer, 0, mainFiel.FieldLength);
                                SdS.Deserialization(bufAnswer, ref answer1);
                                Answer comAnswer = new Answer();
                                comAnswer.mainFiel = mainFiel;
                                comAnswer.obj = answer1;
                                DellArray(ref buffer, 5);
                                index -= 5;
                                return comAnswer;//12 byte
                                */
                            }
                            break;
                        default:
                            DellArray(ref buffer, 2);
                            index -= 2;
                            return null;
                            //break;
                    }
                }
            }
            return null;
        }

        public void DellArray(ref byte[] arrayBytes, int length)
        {
            Array.Clear(arrayBytes, 0, length);
            arrayBytes.Reverse();
            Array.Resize(ref arrayBytes, (arrayBytes.Length - length));
            arrayBytes.Reverse();

        }

        public string FindErr(byte error)
        {
            string messError = "";
            switch (error)
            {
                case 0x00:
                    messError = "Нет ошибки";
                    return messError;
                    //break;
                case 0x01:
                    messError = "Ошибка программная неопределенная";
                    break;
                case 0x02:
                    messError = "Нет ответа от LoRa";
                    break;
                case 0x03:
                    messError = "Нет ответа от другого передатчика по каналу LoRa (250мс) ";
                    break;
                case 0x04:
                    messError = "Ошибка передачи; сделать повторный Init";
                    break;
                case 0x05:
                    messError = "Нет синхронизации";
                    break;
                case 0x06:
                    messError = "Ошибка ввода данных";
                    break;                    
                case 0x0D:
                    messError = "Нет связи с Формирователем";
                    break;
                case 0x10:
                    messError = "ПР - на ЖК при приеме с ПК (внутренние для ЖК)";
                    break;
                case 0x11:
                    messError = "ПРД - на ЖК при передаче на ПК (внутренние для ЖК)";
                    break;
            }
            return messError;
        }


        public void OpenPort(string portName, Int32 baudRate, System.IO.Ports.Parity parity, Int32 dataBits, System.IO.Ports.StopBits stopBits)
        {
            // Open COM port

            if (_port == null)
                _port = new SerialPort();

            // if port is open
            if (_port.IsOpen)

                // close it
                ClosePort();

            // try to open 
            try
            {
                // set parameters of port
                _port.PortName = portName;
                _port.BaudRate = baudRate;

                _port.Parity = parity;
                _port.DataBits = dataBits;
                _port.StopBits = stopBits;

                _port.RtsEnable = true;
                _port.DtrEnable = true;

                //_port.ReadTimeout = 1000;
                //_port.WriteTimeout = 1000;

               // _port.ReceivedBytesThreshold = 1000;
                // set parameters of port
                /*  _port.PortName = portName;
                  _port.BaudRate = baudRate;

                  _port.Parity = System.IO.Ports.Parity.None;
                  _port.DataBits = 8;
                  _port.StopBits = System.IO.Ports.StopBits.One;*/


                // open it
                _port.Open();

                // create the thread for reading data from the port
                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }

                // load function of the thread for reading data from the port
                try
                {
                    thrRead = new Thread(new ThreadStart(ReadData));
                    thrRead.IsBackground = true;
                    thrRead.Start();
                }
                catch (System.Exception)
                {

                }

                // raise event
                // ConnectPort();

                // return true;
                if (OnConnectPort != null)
                    OnConnectPort();
            }
            catch (System.Exception)
            {
                if (OnDisconnectPort != null)
                    OnDisconnectPort();

                //return false;
            }
        }

        public bool PortConnect()
        {
            try
            {

                if (_port.IsOpen)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        // Close COM port
        public void ClosePort()
        {
            // clear in buffer
            try
            {
                _port.DiscardInBuffer();
            }
            catch (System.Exception)
            {


            }

            // clear in buffer
            try
            {
                _port.DiscardOutBuffer();
            }
            catch (System.Exception)
            {

            }

            try
            {
                // close port
                _port.Close();

                // destroy thread of reading
                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }

                if (OnDisconnectPort != null)
                    // raise event
                    OnDisconnectPort();
            }

            catch (System.Exception)
            {

            }

        }


        // Read data (byte) from port (thread)
       /* private void ReadData()
        {
            // buffer for reading bytes
            byte[] bBufRead;
            bBufRead = new byte[256];

            // buffer to transfer and save reading bytes
            byte[] bBufSaveS = null;
            //bBufSaveS = new byte[200];

            // length of reading bytes
            int iReadByte = -1;
            int iBufSave = 0;

            // while port open
            while (true)
            {
                try
                {
                    bBufRead = new byte[256];

                    // clear burre of reading
                    Array.Clear(bBufRead, 0, bBufRead.Length);

                    iReadByte = 0;

                    // read data
                    iReadByte = _port.Read(bBufRead, 0, bBufRead.Length);

                    // if bytes was read
                    if (iReadByte > 0)
                    {
                        Array.Resize(ref bBufRead, iReadByte);

                        if (OnReadByte != null)
                            OnReadByte(bBufRead);

                        if (bBufSaveS == null)
                            bBufSaveS = new byte[iReadByte];
                        else
                            Array.Resize(ref bBufSaveS, bBufSaveS.Length + iReadByte);


                        Array.Copy(bBufRead, 0, bBufSaveS, bBufSaveS.Length - iReadByte, iReadByte);
                        iBufSave += iReadByte;
                        try
                        {
                            while (iBufSave >= 6)
                            {
                                MainFiel mainFiel = new MainFiel();
                                mainFiel.Address = bBufSaveS[0];
                                mainFiel.cipher = bBufSaveS[1];
                                mainFiel.FieldLength = bBufSaveS[2];

                                if (iBufSave >= LEN_HEAD + mainFiel.FieldLength)
                                {
                                    byte[] bData = new byte[LEN_HEAD + mainFiel.FieldLength];

                                    Array.Copy(bBufSaveS, 0, bData, 0, LEN_HEAD + mainFiel.FieldLength);
                                    int iLen = LEN_HEAD + mainFiel.FieldLength;
                                    byte[] answers = FindCmd(ref bData, ref iLen);

                                    Array.Reverse(bBufSaveS);
                                    Array.Resize(ref bBufSaveS, bBufSaveS.Length - (LEN_HEAD + mainFiel.FieldLength - iLen));
                                    Array.Reverse(bBufSaveS);

                                    iBufSave -= (LEN_HEAD + mainFiel.FieldLength);

                                    if (answers != null)
                                    {
                                        if (OnReceiveCmd != null)
                                        {
                                            OnReceiveCmd(answers);
                                        }

                                    }

                                }
                            }
                        }
                        catch (Exception)
                        {

                        }


                    }

                    else
                        if (OnDisconnectPort != null)
                            OnDisconnectPort();
                }

                catch (System.Exception ex)
                {

                    ClosePort();

                }

                Thread.Sleep(5);
            }
        }*/
        private void ReadData()
        {
            // buffer for reading bytes
            byte[] bBufRead;
            bBufRead = new byte[256];

            // buffer to transfer and save reading bytes
            byte[] bBufSaveS;
            bBufSaveS = null;

            // length of reading bytes
            int iReadByte = -1;
            int iBufSave = 0;

            // while port open
            while (true)
            {
                try
                {
                    bBufRead = new byte[256];

                    // clear burre of reading
                    Array.Clear(bBufRead, 0, bBufRead.Length);

                    iReadByte = 0;

                    // read data
                    iReadByte = _port.Read(bBufRead, 0, bBufRead.Length);

                    // if bytes was read
                    if (iReadByte > 0)
                    {
                        Array.Resize(ref bBufRead, iReadByte);

                        if (OnReadByte != null)
                            OnReadByte(bBufRead);

                        if (bBufSaveS == null)
                            bBufSaveS = new byte[iReadByte];
                        else
                            Array.Resize(ref bBufSaveS, bBufSaveS.Length + iReadByte);

                        Array.Copy(bBufRead, 0, bBufSaveS, bBufSaveS.Length - iReadByte, iReadByte);
                        iBufSave += iReadByte;
                        bool Contin = true;
                        while (iBufSave >= 5 && Contin)
                        {
                            MainFiel mainFiel = new MainFiel();
                            mainFiel.Address = bBufSaveS[0];
                            mainFiel.cipher = bBufSaveS[1];
                            mainFiel.FieldLength = bBufSaveS[2];
                            if (iBufSave >= LEN_HEAD + mainFiel.FieldLength)
                            {
                                byte[] bData = new byte[LEN_HEAD + mainFiel.FieldLength];

                                Array.Copy(bBufSaveS, 0, bData, 0, LEN_HEAD + mainFiel.FieldLength);
                                int iLen = LEN_HEAD + mainFiel.FieldLength;

                                byte[] answer = FindCmd(ref bData, ref iLen);

                                Array.Reverse(bBufSaveS);
                                Array.Resize(ref bBufSaveS, bBufSaveS.Length - (LEN_HEAD + mainFiel.FieldLength));
                                Array.Reverse(bBufSaveS);

                                iBufSave -= (LEN_HEAD + mainFiel.FieldLength);
                                if (answer != null)
                                {
                                    if (OnReceiveCmd != null)
                                    {
                                        OnReceiveCmd(answer);
                                    }

                                }
                            }
                            else 
                            {
                                Contin = false;
                            }
                        }

                    }

                    else
                        if (OnDisconnectPort != null)
                            OnDisconnectPort();
                }

                catch (System.Exception)
                {

                    ClosePort();

                }

                Thread.Sleep(5);
            }
        }

        public bool SendAnswer(byte code, byte T, byte H, byte ADC, byte error, byte address) // length information  5 byte
        {
            switch (code)
            {
                case 0xF1:
                    MainFiel mainFiel;
                    mainFiel.Address = address;
                    mainFiel.cipher = code;
                    mainFiel.FieldLength = 5;
                    byte[] answer = SdS.Serialization(mainFiel);
                    int i = answer.Length;
                    Array.Resize(ref answer, answer.Length + 3);
                    answer[i++] = T;
                    answer[i++] = H;
                    answer[i++] = ADC;
                    byte[] crcSpecial = CRC16(answer, (ushort) (mainFiel.FieldLength + 1));
                    i = answer.Length;
                    Array.Resize(ref answer, answer.Length + 2);
                    answer[i++] = crcSpecial[1];
                    answer[i] = crcSpecial[0];
                    return WriteData(answer);
                    //break;
                default:
                    MainFiel mainFiels;
                    mainFiels.Address = address;
                    mainFiels.cipher = code;
                    mainFiels.FieldLength = 3;
                    byte number = error;
                    byte[] answers = SdS.Serialization(mainFiels);
                    int j = answers.Length;
                    Array.Resize(ref answers, answers.Length + 1);
                    answers[j++] = number;
                    byte[] crc = CRC16(answers, (ushort)(mainFiels.FieldLength + 1));
                    j = answers.Length;
                    Array.Resize(ref answers, answers.Length + 2);
                    answers[j++] = crc[1];
                    answers[j] = crc[0];
                    return WriteData(answers);
                   // break;
            }

        }

        public void SendPPRCH(int frequency, byte numberFreq, byte step, Int16 seltusFreq)
        {

        }
        
        private bool WriteData(byte[] bSend)
        {
            try
            {
                _port.Write(bSend, 0, bSend.Length);

                if (OnWriteByte != null)
                    OnWriteByte(bSend);

                return true;
            }
            catch (System.Exception)
            {
                return false;
            }

        }
        public byte[] CRC16(byte[] Buf, ushort Len)
        {
            const ushort polinom = 0x1021;
            ushort Result = 0xFFFF;

            for (int i = 0; i < Len; ++i)
            {
                Result ^= (ushort)(Buf[i] << 8);
                for (uint j = 0; j < 8; ++j)
                {
                    Result >>= 1;
                    if ((Result & 0x01) != 0) Result ^= polinom;
                }
            }
            byte[] result = BitConverter.GetBytes(Result);
            return result;
        }
    }
}
