﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using CNT;
using Ini;
using System.IO.Ports;
using System.IO;
using System.Xml;

namespace Heterodyne
{
    public partial class FormHeter : Form
    {
        const double MinFreq = 1.00d;
        const double MaxFreq = 6000.00d;
        const int MinStepPprch = 25;
        const int MaxStepPprch = 1000;
        const int MinSaltusPprch = 2;
        const int MaxSaltusPprch = 2000;
        public bool flagOFF = false;
        private bool flagFirstOpen;
        byte address;
        byte code;
        string[] NumberStrip;
        const string sRusL = "rus";
        const string sEng = "eng";
        const string sAzL = "az";
        int iLangugeSwitch = 0;
        string sLanguage = sRusL;
        string sCodeMessFromShaper = "";
        string sTemp = "";
        string sHumidid = "";
        string sCharge = "";
        string SelectedTabName = "OtherCode";
       // bool SpecilFlag = false;

        IniFile ini;
        string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

        private byte[] NumberFreq = {2, 4, 8, 16, 32, 64, 121};
        private byte[] ArrayStepFreq = { 1, 2, 5, 10 };
        public int NumberFileFreq = 0;
     
        ComHTRD comHTRD;
        ComShaper comShaper;
        static ushort usTimeIndicate = 300;
        static ushort usTimeSend = 1000;
        static ushort PollTimeDelay = 0;
        static ushort usTimeDelay = 0;
        //bool flagEnterMess = true; //флаг для повторной отправки кодограм в случае отсутствия ответа
        int CountMaxEnter;

        System.Threading.Timer tmWriteByteHTRD;
        System.Threading.Timer tmReadByteHTRD;
        System.Threading.Timer tmDurationRadiationHTRD;
        System.Threading.Timer tmAutoEnter;


        public delegate void dlgShowLogMessage(byte bType, string strTimeCodeAdr, string strInfoMessage, string strTypeCmd);
        public dlgShowLogMessage _ShowLogMessage;

        Dictionary<string, string> TranslateDic;

        int CurrentFreq = 0;
        int CurrentStep = 0;
        int MaxCountReq = 7;

        public FormHeter()
        {
            InitializeComponent();
            
        }

        private void InitConnectionHTRD()
        {
            if (comHTRD != null)
                comHTRD = null;
            comHTRD = new ComHTRD();

            
            comHTRD.OnConnectPort += new ComHTRD.ConnectEventHandler(ShowConnectHTRD);
            comHTRD.OnDisconnectPort += new ComHTRD.ConnectEventHandler(ShowDisconnectHTRD);
            comHTRD.OnReceiveCmd += new ComHTRD.CmdEventHandler(ShowReceiveCmd);

            // read array of byte
            comHTRD.OnReadByte += new ComHTRD.ByteEventHandler(ShowReadByteHTRD_My);

            // write array of byte
            comHTRD.OnWriteByte += new ComHTRD.ByteEventHandler(ShowWriteByteHTRD);

            if (ComPortName.SelectedIndex >= 0 && ComSpeed.SelectedIndex >= 0)
            {
                comHTRD.OpenPort(ComPortName.Items[ComPortName.SelectedIndex].ToString(),
                    Convert.ToInt32(ComSpeed.Items[ComSpeed.SelectedIndex]), Parity.None, 8,
                    StopBits.One);
            }
            if (Convert.ToByte(ini.IniReadValue("View", "FileFreq")) == 0)
                FileFreqCode.Parent = null;//вкладка сетка частот по просьбе Минчина, 19.01.2018

            if (Convert.ToByte(ini.IniReadValue("View", "PollCode")) == 0)
                PollCode.Parent = null;
            usTimeSend = Convert.ToUInt16(ini.IniReadValue("Poll", "TimeSend"));
            MaxCountReq = Convert.ToInt16(ini.IniReadValue("Poll", "MaxCountReq"));
            PollTimeDelay = Convert.ToUInt16(ini.IniReadValue("Poll", "TimeDelay"));
        }

        private void InitDelegate()
        {
            _ShowLogMessage = new dlgShowLogMessage(ShowLogMessage);
 
        }


        private void ShowReadByteHTRD_My(byte[] bData)
        {
            if (pbReadHTRD.InvokeRequired)
            {
                pbReadHTRD.Invoke((MethodInvoker)(delegate()
                {
                    pbReadHTRD.Image = Properties.Resources.green;
                    tmReadByteHTRD = new System.Threading.Timer(TimeReadByteHTRD, null, usTimeIndicate, 0);
                }));

            }
            else
            {
                pbReadHTRD.Image = Properties.Resources.green;
                tmReadByteHTRD = new System.Threading.Timer(TimeReadByteHTRD, null, usTimeIndicate, 0);
            }

            if (AllBytesBox.Checked == true)
            {
                ShowByteHTRD(bData);
            }
 
        }
        private void ShowByteHTRD(byte[] bData)
        {

            string mess = "";
            if (bData != null)
            {
                for (int i = 0; i < bData.Length; i++)
                    mess += bData[i].ToString("X2") + " ";
            }
            mess += "\n";
            ShowLogMessage(0, "", mess, "");
 
        }
       

        private void bReceiverChannel_Click(object sender, EventArgs e)
        {
            if (bChannelHTRD.BackColor == Color.Red)
            {
                if (TabControl.SelectedTab == Comp)
                {
                    InitConnectionHTRD();
                }
                else
                {
                    InitConnectionShaper();
                }
            }
            else
            {
                if (comHTRD != null)
                {

                    comHTRD.ClosePort();
                }
                if (comShaper != null)
                {
                    comShaper.ClosePort();
                }
            }
        }


        private void ShowReceiveCmd(object structure)
        {
            Answer partAnswer = (Answer) structure;
            LastPartAnswer lastPart;
            try
            {
                lastPart = (LastPartAnswer)partAnswer.obj;
                if (lastPart.ErrorCode != 0x04)
                {
                    Invoke((MethodInvoker)(() => tmAutoEnter.Dispose()));
                    CountMaxEnter = 0;
                    timerError.Enabled = false;
                    timerEnabled.Enabled = false;
                    ShowStateButton(bSendALX, true);
                    ShowStateButton(OnButton, true);
                    ShowStateButton(OffButton, true);
                }
            }
            catch
            {
                Invoke((MethodInvoker)(() => tmAutoEnter.Dispose()));
                CountMaxEnter = 0;
                timerError.Enabled = false;
                timerEnabled.Enabled = false;
                ShowStateButton(bSendALX, true);
                ShowStateButton(OnButton, true);
                ShowStateButton(OffButton, true); 
            }
            int sizeInBytes = System.Runtime.InteropServices.Marshal.SizeOf(structure);
            string mess="";
            string messError = "";          
            if (ErrorLabel.InvokeRequired)
            {
                ErrorLabel.Invoke((MethodInvoker)(delegate()
                {
                    ErrorLabel.Text = TranslateDic[ErrorLab.Name];// "Cообщение:";
                }));

            }
            else
            {
                ErrorLabel.Text = TranslateDic[ErrorLab.Name]; //"Сообщение:";
            }
            if (Error.InvokeRequired)
            {
                Error.Invoke((MethodInvoker)(delegate()
                {
                    Error.Image = Properties.Resources.gray;
                }));

            }
            else
            {
                Error.Image = Properties.Resources.gray;
            }

            switch (partAnswer.mainFiel.cipher)
            {
                case ComHTRD.FRCH:
                    LastPartAnswer answerFM = (LastPartAnswer)partAnswer.obj;

                    FreqPoll(answerFM.ErrorCode);

                    byte[] crcFM = BitConverter.GetBytes(answerFM.CRC);
                    mess = partAnswer.mainFiel.Address.ToString("X2") + " " + partAnswer.mainFiel.cipher.ToString("X2") + " " +
                           partAnswer.mainFiel.FieldLength.ToString("X2") + " " + answerFM.ErrorCode.ToString("X2") + " " +
                           crcFM[0].ToString("X2") + " " + crcFM[1].ToString("X2") + "\n";
                    ShowLogMessage(1, "", mess, "");
                    messError = TranslateDic[answerFM.ErrorCode.ToString("X2")];
                    sCodeMessFromShaper = answerFM.ErrorCode.ToString("X2");
                    string messageFM = TranslateDic[ErrorLabel.Name] + messError; // "Сообщение:  " + messError;
                    ShowInfoLabel(ErrorLabel, messageFM);

                    if (answerFM.ErrorCode != 0x00)
                    {
                        if (Error.InvokeRequired)
                        {
                            Error.Invoke((MethodInvoker)(() => Error.Image = Properties.Resources.red));
                        }
                        else
                        {
                            Error.Image = Properties.Resources.red;
                        }
                    }
                    else
                    {
                        if (Error.InvokeRequired)
                        {
                            Error.Invoke((MethodInvoker)(() => Error.Image = Properties.Resources.gray));
                        }
                        else
                        {
                            Error.Image = Properties.Resources.gray;
                        }

                    }
                    if (answerFM.ErrorCode == 0x00 && flagOFF == false)
                    {
                        Invoke((MethodInvoker)(() => Power.Image = Properties.Resources.green));
                        if (Duration.Value != 0)
                            tmDurationRadiationHTRD = new System.Threading.Timer(TimeDurationRadiationHTRD, null, Convert.ToUInt16(Duration.Value) * 1000, 0);

                    }
                    else
                    {
                        Invoke((MethodInvoker)(() => Power.Image = Properties.Resources.gray));
                        flagOFF = false;
                    }

                    break;

                case ComHTRD.OffSignal: case ComHTRD.FM: case ComHTRD.AM: case ComHTRD.CHM: case ComHTRD.FielFreq: case ComHTRD.PPRCH:
                    /*
                    Invoke((MethodInvoker)(() => tmAutoEnter.Dispose()));
                    CountMaxEnter = 0;
                    timerError.Enabled = false;
                    timerEnabled.Enabled = false;
                    ShowStateButton(bSendALX, true);
                    ShowStateButton(OnButton, true);
                    ShowStateButton(OffButton, true);
                    */
                    LastPartAnswer answer = (LastPartAnswer)partAnswer.obj;
                    byte[] crc = BitConverter.GetBytes(answer.CRC);
                    mess = partAnswer.mainFiel.Address.ToString("X2") + " " + partAnswer.mainFiel.cipher.ToString("X2") + " " +
                           partAnswer.mainFiel.FieldLength.ToString("X2") + " " + answer.ErrorCode.ToString("X2") + " " +
                           crc[0].ToString("X2") + " " + crc[1].ToString("X2") + "\n";
                    ShowLogMessage(1,"",mess,"");
                    if (answer.ErrorCode == comHTRD.listError[0] && flagOFF == true)
                    {
                        messError = TranslateDic["Mess_OFF"];// "Сигнал выключен"; 
                        sCodeMessFromShaper = "Mess_OFF";
                    }
                    else
                    {
                    messError = TranslateDic[answer.ErrorCode.ToString("X2")];
                    sCodeMessFromShaper = answer.ErrorCode.ToString("X2");
                    }
                    string message =TranslateDic[ErrorLabel.Name] + messError; // "Сообщение:  " + messError;
                    ShowInfoLabel(ErrorLabel, message);
                    
                    if (answer.ErrorCode != 0x00)
                    {
                        if (Error.InvokeRequired)
                        {
                            Error.Invoke((MethodInvoker)(() => Error.Image = Properties.Resources.red));
                        }
                        else
                        {
                            Error.Image = Properties.Resources.red;
                        }
                    }
                    else
                    {
                        if (Error.InvokeRequired)
                        {
                            Error.Invoke((MethodInvoker)(() => Error.Image = Properties.Resources.gray));
                        }
                        else
                        {
                            Error.Image = Properties.Resources.gray;
                        }

                    }
                    if (answer.ErrorCode == 0x00 && flagOFF == false)
                    {
                        Invoke((MethodInvoker)(() => Power.Image = Properties.Resources.green));
                        if (Duration.Value != 0)
                        tmDurationRadiationHTRD = new System.Threading.Timer(TimeDurationRadiationHTRD, null, Convert.ToUInt16(Duration.Value)*1000, 0);

                    }
                    else
                    {
                        Invoke((MethodInvoker)(() => Power.Image = Properties.Resources.gray));
                        flagOFF = false;
                    }

                    break;
                case ComHTRD.SpecialCipher:
                    
                    Invoke((MethodInvoker)(() => tmAutoEnter.Dispose()));
                    CountMaxEnter = 0;
                    timerError.Enabled = false;
                    timerEnabled.Enabled = false;
                    ShowStateButton(bSendALX, true);
                    ShowStateButton(OnButton, true);
                    ShowStateButton(OffButton, true);

                    message = TranslateDic[ErrorLabel.Name]; //"Сообщение:  ";
                    ShowInfoLabel(ErrorLabel, message);
                    SpecialPartAnswer specialAnswer = (SpecialPartAnswer)partAnswer.obj;
                    crc = new byte[2];
                    crc = BitConverter.GetBytes(specialAnswer.CRC);
                    mess = partAnswer.mainFiel.Address.ToString("X2") + " " + partAnswer.mainFiel.cipher.ToString("X2") + " " +
                           partAnswer.mainFiel.FieldLength.ToString("X2") + " " + specialAnswer.T.ToString("X2") + " " +
                           specialAnswer.H.ToString("X2") + " " + specialAnswer.Charge.ToString("X2") + " " +
                           crc[0].ToString("X2") + " " + crc[1].ToString("X2") + "\n";
                    ShowLogMessage(1,"",mess,"");

                    message = TranslateDic[Templabel.Name] + specialAnswer.T.ToString() + " " + "\u00B0" + "C";
                    sTemp = specialAnswer.T.ToString() + " " + "\u00B0" + "C";
                //"Температура " + specialAnswer.T.ToString() + " " + "\u00B0" + "C";
                    ShowInfoLabel(Templabel, message);
                    message = TranslateDic[Humididylabel.Name] + specialAnswer.H.ToString() + " " + "\u0025";
                    sHumidid = specialAnswer.H.ToString() + " " + "\u0025";
                //"Влажность " + specialAnswer.H.ToString() + " " + "\u0025";
                    ShowInfoLabel(Humididylabel, message);
                    message = TranslateDic[Chargelabel.Name] + (ADC_battery(specialAnswer.Charge)).ToString() + " " + "V";
                    sCharge = (ADC_battery(specialAnswer.Charge)).ToString() + " " + "V";
                //"Заряд АКБ " + (ADC_battery(specialAnswer.Charge)).ToString() + " " + "B";
                    if (ADC_battery(specialAnswer.Charge) <= 13.5)
                    {
                        ShowStateLabel(Chargelabel, Color.Red);
                    }
                    else
                    {
                        ShowStateLabel(Chargelabel, Color.WhiteSmoke);
                    }
                    ShowInfoLabel(Chargelabel, message);
                    break;
                    
            }
            
        }

        double ADC_battery(byte charge)
        {
            double adc_battery = (16.8d * charge) / 155.0d;
            return Math.Round(adc_battery,2);
        }

        struct Item
        {
            public Int32 frequency;
            public byte Code;
            public byte numberFreq;
            public byte step;
            public Int16 saltusFreq;
            public byte time;

            public Item(Int32 frequency, byte Code, byte numberFreq, byte step, Int16 saltusFreq, byte time) 
            {
                this.frequency = frequency;
                this.Code = Code;
                this.numberFreq = numberFreq;
                this.step = step;
                this.saltusFreq = saltusFreq;
                this.time = time;
            }
        }
        Item item;
        public void SpecialSend(Int32 frequency, byte Code, byte numberFreq, byte step, Int16 saltusFreq, byte time) 
        {
            item = new Item(frequency, Code, numberFreq, step, saltusFreq, time);
            tmAutoEnter = new System.Threading.Timer(TimeAutoSentByteHTRD, item, usTimeDelay, usTimeSend);
        }

        private void TimeAutoSentByteHTRD(object o)
        {
            //Invoke((MethodInvoker)(() => comHTRD.Send(item.frequency, item.Code, item.numberFreq, item.step, item.saltusFreq, item.time)));
            comHTRD.Send(item.frequency, item.Code, item.numberFreq, item.step, item.saltusFreq, item.time);
            CountMaxEnter++;
            if (CountMaxEnter >= MaxCountReq)
            {
                CountMaxEnter = 0;
                tmAutoEnter.Dispose(); 
            }

        }
        private void ShowWriteByteHTRD(byte[] bData)
        {            
            
            if (pbWriteHTRD.InvokeRequired)
            {
                pbWriteHTRD.Invoke((MethodInvoker)(delegate()
                {
                    pbWriteHTRD.Image = Properties.Resources.red;
                    tmWriteByteHTRD = new System.Threading.Timer(TimeWriteByteHTRD, null, usTimeIndicate, 0);
                    //WriteData
                }));

            }
            else
            {
                pbWriteHTRD.Image = Properties.Resources.red;
                tmWriteByteHTRD = new System.Threading.Timer(TimeWriteByteHTRD, null, usTimeIndicate, 0);
            }

            timerError.Enabled = true;
            //ShowStateButton(OnButton, false);
            //ShowStateButton(OffButton, false);
            ShowStateButton(OnButton, true);
            ShowStateButton(OffButton, true);

            ShowWtiteDByte(bData);

        }

        // 
        private void ShowReadByteHTRD(byte[] bData)
        {    
                        
            try
            {
                if (pbReadHTRD.InvokeRequired)
                {
                    pbReadHTRD.Invoke((MethodInvoker)(delegate()
                    {
                        pbReadHTRD.Image = Properties.Resources.green;
                        tmReadByteHTRD = new System.Threading.Timer(TimeReadByteHTRD, null, usTimeIndicate, 0);
                    }));

                }
                else
                {
                    pbReadHTRD.Image = Properties.Resources.green;
                    tmReadByteHTRD = new System.Threading.Timer(TimeReadByteHTRD, null, usTimeIndicate, 0);
                }


                ShowWtiteDByte(bData);
            }
            catch(SystemException)
            {}

        }

        private void ShowWtiteDByte(byte[] bByte)
        {

            string hex = BitConverter.ToString(bByte);
            hex = hex.Replace("-", " ");
            hex += "\n";
            ShowLogMessage(5, hex, "", "");
        }


        // show read byte
        private void ShowWriteByte(byte[] bByte)
        {

            string hex = BitConverter.ToString(bByte);
            hex = hex.Replace("-", " ");
            hex += "\n";
            ShowLogMessage(3, hex, "", "");
        }
        // 
        private void TimeWriteByteHTRD(object o)
        {
            if (pbWriteHTRD.InvokeRequired)
            {
                pbWriteHTRD.Invoke((MethodInvoker)(delegate()
                {
                    pbWriteHTRD.Image = Properties.Resources.gray;
                    tmWriteByteHTRD.Dispose();
                }));

            }
            else
            {
                pbWriteHTRD.Image = Properties.Resources.gray;
                tmWriteByteHTRD.Dispose();
            }

        }

        // 
        private void TimeReadByteHTRD(object o)
        {
            if (pbReadHTRD.InvokeRequired)
            {
                pbReadHTRD.Invoke((MethodInvoker)(delegate()
                {
                    pbReadHTRD.Image = Properties.Resources.gray;
                    tmReadByteHTRD.Dispose();
                }));

            }
            else
            {
                pbReadHTRD.Image = Properties.Resources.gray;
                tmReadByteHTRD.Dispose();
            }
        }

        private void TimeDurationRadiationHTRD(object o)
        {
            if (Power.InvokeRequired)
            {
                Power.Invoke((MethodInvoker)(delegate()
                {
                    Power.Image = Properties.Resources.gray;
                    tmDurationRadiationHTRD.Dispose();
                }));

            }
            else
            {
                Power.Image = Properties.Resources.gray;
                tmDurationRadiationHTRD.Dispose();
            }
        }

        // show info on label
        private void ShowInfoLabel(Label label, string strInfo)
        {
            if (label.InvokeRequired)
            {
                label.Invoke((MethodInvoker)(delegate()
                {
                    label.Text = strInfo;
                }));

            }
            else
            {
                label.Text = strInfo;
            }
        }
        // show state on label
        private void ShowStateLabel(Label label, Color color)
        {
            if (label.InvokeRequired)
            {
                label.Invoke((MethodInvoker)(delegate()
                {
                    label.BackColor = color;
                }));

            }
            else
            {
                label.BackColor = color;
            }
        }
        //show state button
        private void ShowStateButton(Button button, bool state)
        {
            if (button.InvokeRequired)
            {
                button.Invoke((MethodInvoker)(delegate()
                {
                    button.Enabled = state;
                }));

            }
            else
            {
                button.Enabled = state;
            }
        }
        
        //show color button
        private void ShowColorButton(Button button, Color color)
        {
            if (button.InvokeRequired)
            {
                button.Invoke((MethodInvoker)(delegate()
                {
                    button.BackColor = color;
                }));

            }
            else
            {
                button.BackColor = color;
            }
        }


        private void ShowConnectHTRD()
        {
            ShowStateButton(bSendALX, true);
            ShowColorButton(bChannelHTRD, Color.Green);
            if (TabControl.SelectedTab == Comp)
            {
                ini = new IniFile(path + "\\Resources" + "\\SettingsHTRD.ini");
            }
            else
            {
                ini = new IniFile(path + "\\Resources" + "\\SettingsShaper.ini");
            }
            ini.IniWriteValue("COM", "Name", ComPortName.Items[ComPortName.SelectedIndex].ToString());
            ini.IniWriteValue("COM", "Speed", ComSpeed.Items[ComSpeed.SelectedIndex].ToString());
            timerOpen.Enabled = true;
        }

        private void ShowDisconnectHTRD()
        {
            ShowStateButton(OnButton, false);
            ShowStateButton(OffButton, false);
            ShowStateButton(bSendALX, false);
            ShowColorButton(bChannelHTRD, Color.Red);
        }


        // show read/write command message
        private void ShowLogMessage(byte bType, string strTimeCodeAdr, string strInfoMessage, string strTypeCmd)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(_ShowLogMessage, new object[] { bType, strTimeCodeAdr, strInfoMessage, strTypeCmd });
            }
            else
            {
                try
                {
                    if (rtbLog.TextLength == rtbLog.MaxLength - 100)
                        rtbLog.Clear();

                    rtbLog.AppendText("\n");
                    switch (bType)
                    {
                        case 0:
                            rtbLog.SelectionColor = Color.Purple;
                            break;

                        case 1:
                            rtbLog.SelectionColor = Color.Red;
                            break;

                        case 2:
                            rtbLog.SelectionColor = Color.Blue;
                            break;

                        case 3:
                            rtbLog.SelectionColor = Color.Blue;
                            break;

                        case 4:
                            rtbLog.SelectionColor = Color.Red;
                            break;

                        default:
                            rtbLog.SelectionColor = Color.Black;
                            break;
                    }

                    rtbLog.AppendText(strTimeCodeAdr);
                    rtbLog.SelectionColor = Color.Black;
                    rtbLog.AppendText(strTypeCmd); switch (bType)
                    {
                        case 0:
                            rtbLog.SelectionColor = Color.Purple;
                            break;

                        case 1:
                            rtbLog.SelectionColor = Color.Red;
                            break;

                        case 2:
                            rtbLog.SelectionColor = Color.Blue;
                            break;

                        case 3:
                            rtbLog.SelectionColor = Color.Blue;
                            break;

                        case 4:
                            rtbLog.SelectionColor = Color.Red;
                            break;

                        default:
                            rtbLog.SelectionColor = Color.Black;
                            break;
                    }
                    rtbLog.AppendText(strInfoMessage);
                    rtbLog.SelectionStart = rtbLog.TextLength;
                    rtbLog.ScrollToCaret();
                }
                catch (System.Exception ex)
                {
                    //GlobalVar._LogFile.LogWrite("   " + this.Name + ":  " + ex.TargetSite + "   " + ex.Message); 
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error,
                        MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                }
            }
        }


        private void tmrAutoSendALX_Tick(object sender, EventArgs e)
        {
            if (comHTRD != null)
                SpecialSend(0, ComHTRD.SpecialCipher, 0, 0, 0, 0);  //comHTRD.Send(0, ComHTRD.SpecialCipher, 0, 0, 0, 0);
        }
        
        private void pStateFCh_Paint(object sender, PaintEventArgs e)
        {

        }
        private void SetParam()
        {
            bSendALX.Enabled = false;
            ShowStateButton(OnButton, false);
            ShowStateButton(OffButton, false);
            //ShowStateButton(OnButton, true);
            //ShowStateButton(OffButton, true);

            SendSpecialCommand.Enabled = false;
            flagFirstOpen = true;
            string[] portnames = SerialPort.GetPortNames();
            ComPortName.Items.AddRange(portnames);
            //NumFreq.Items.AddRange(NumberFreq);
            ini = new IniFile(path + "\\Resources" + "\\SettingsHTRD.ini");
            timerEnabled.Interval = Convert.ToInt32(ini.IniReadValue("Parameters", "timerEnabled"));
            tmrAutoSendALX.Interval = Convert.ToInt32(ini.IniReadValue("Parameters", "Interval"));
            StartFreq.Text = ini.IniReadValue("Parameters", "StartFreq");
            StartFreqPPRCH.Text = ini.IniReadValue("Parameters", "StartFreq");
            StepBox.Text = ini.IniReadValue("Parameters", "StepFreq");
            StepStartFreq.Text = ini.IniReadValue("Parameters", "StepFreq");
            NumFreq.SelectedIndex = NumFreq.FindString(ini.IniReadValue("Parameters", "NumberFreq"));
            StepPPRCHBox.Text = ini.IniReadValue("Parameters", "Step");
            SaltusBox.Text = ini.IniReadValue("Parameters", "SaltusFreq");
            usTimeSend = Convert.ToUInt16(ini.IniReadValue("Parameters", "TimeEnter"));
            iLangugeSwitch = Convert.ToByte(ini.IniReadValue("Parameters", "LangugeSwitch "));
            sLanguage = Convert.ToByte(ini.IniReadValue("Parameters", "Language ")) == 0 ? sRusL : (iLangugeSwitch == 0? sAzL : sEng);
            LangBut.Text = sLanguage == sRusL ? "Ru": (iLangugeSwitch == 0 ? "Az" : "EN");
            TypeFreqBox.SetItemChecked(0, true);
            TypeFreqBox.SelectedIndex = 0;
            StreamReader Str = new StreamReader(path + "\\Resources" + "\\NumberStrip.txt");
            NumberStrip = Str.ReadToEnd().Split('\r');
            string subString = "\n";
            int indexOfSubstring;
            for (int i = 0; i < NumberStrip.Length; i++)
            {
                indexOfSubstring = NumberStrip[i].IndexOf(subString);

                if (indexOfSubstring == 0) { NumberStrip[i] = NumberStrip[i].Substring(indexOfSubstring + subString.Length); }
                else
                {
                    if (indexOfSubstring > 0)
                    {
                        NumberStrip[i] = NumberStrip[i].Substring(0, NumberStrip[i].Length - subString.Length);
                    }
                }
            }
            EPO.Maximum = NumberStrip.Length;
            label_NumStrip.Text = NumberStrip[0];
            StepFileFreq.SelectedIndex = 0;

            if (Convert.ToByte(ini.IniReadValue("View", "OnlyCom")) == 1)
                CompButton_Click(null, new EventArgs()); //По просьбе минчина
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            SetParam();
            if (sLanguage != sRusL)
                ChangaLanguage();
            else
                LoadDictionary();
        }

        private void ComPortName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (flagFirstOpen == false)
            {
                if (bChannelHTRD.BackColor == Color.Red)
                {
                    if (TabControl.SelectedTab == Comp)
                    {
                        InitConnectionHTRD();
                    }
                    else
                    {
                        InitConnectionShaper();
                    }
                }
                else
                {
                    if (comHTRD != null)
                    {

                        comHTRD.ClosePort();
                        InitConnectionHTRD();
                    }
                    if (comShaper != null)
                    {
                        comShaper.ClosePort();
                        InitConnectionShaper();
                    }
                }
                /*if (SpecilFlag == true && TabControl.SelectedTab == Comp)
                {
                    InitConnectionHTRD();
                }
                else // TabControl.SelectedTab = Shaper
                {
                    InitConnectionShaper();
                }*/
            }

        }

        private void UpDate_Click(object sender, EventArgs e)
        {
            timerOpen.Enabled = false;
            ComPortName.Text = "ComName";
            if (bChannelHTRD.BackColor != Color.Red)
            {
                try
                {
                    if (comHTRD != null)
                    {
                        comHTRD.ClosePort();
                    }
                    if (comShaper != null)
                    {
                        comShaper.ClosePort();
                    }
                }
                catch
                {
                    ShowDisconnectHTRD();
                }
                }
            string[] portnames = SerialPort.GetPortNames();
            int indexName;
            ComPortName.Items.Clear();
            ComPortName.Items.AddRange(portnames);
            if (TabControl.SelectedTab == Comp)
            {
                ini = new IniFile(path + "\\Resources" + "\\SettingsHTRD.ini");
            }
            else
            {
                ini = new IniFile(path + "\\Resources" + "\\SettingsShaper.ini");
            }
            indexName = ComPortName.FindString(ini.IniReadValue("COM", "Name"));
            if (indexName > 0  && ini.IniReadValue("COM", "Name") != "" && Array.IndexOf(portnames, ComPortName.Items[indexName])>=0)
            {
                ComPortName.SelectedIndex = indexName;
                
            }
        }
        
        private void bSendALX_Click_1(object sender, EventArgs e)
        {
            timerError.Enabled = true;
            bSendALX.Enabled = false;
            timerEnabled.Enabled = true;
            try
            {
                Templabel.Text = TranslateDic[Templabel.Name];
                sTemp = "";
            }
            catch { Templabel.Text = "Температура "; }
            try
            {
                Humididylabel.Text = TranslateDic[Humididylabel.Name];
                sHumidid = "";
            }
            catch { Humididylabel.Text = "Влажность "; }
            try
            {
                Chargelabel.Text = TranslateDic[Chargelabel.Name];
                sCharge = "";
            }
            catch { Chargelabel.Text = "Заряд АКБ " ; } 
            try
            {
                ErrorLabel.Text = TranslateDic[ErrorLabel.Name];
                sCodeMessFromShaper = "";
            }
            catch { ErrorLabel.Text = "Сообщение:  "; }            
            Power.Visible = true;
            Power.Image = Properties.Resources.gray;
            Error.Image = Properties.Resources.gray;
            if (comHTRD != null)
            {
                SpecialSend(0, ComHTRD.SpecialCipher, 0, 0, 0, 0); 
                //comHTRD.Send(0, ComHTRD.SpecialCipher, 0, 0, 0, 0);
                //tmrAutoSendALX.Interval = Convert.ToInt32(20*1000);
                SendSpecialCommand.Enabled = true;
               // tmrAutoSendALX.Enabled = true;
                //SendSpecialCommand.BackColor = Color.Aquamarine;
            }
            else
            {
                MessageBox.Show("Отсутствует подключение");
                bChannelHTRD.Focus();
            }


        }

        private void chbSendALX_CheckedChanged_1(object sender, EventArgs e)
        {
        }

        private void nudIntervalSendALX_ValueChanged_1(object sender, EventArgs e)
        {
            
        }

        private void CompButton_Click(object sender, EventArgs e)
        {
            TabControl.SelectedTab = Comp;
            panelStart.Visible = false;
            pStateFCh.Visible = true;
            TabControl.Visible = true;

            Shaper.Parent = null;

            //Shaper.Enabled = false;

            //SpecilFlag = true;
            if (TabControl.SelectedTab == Comp)
             {
                 ini = new IniFile(path + "\\Resources" + "\\SettingsHTRD.ini");
             }
             else
             {
                 ini = new IniFile(path + "\\Resources" + "\\SettingsShaper.ini");
             }
             if (ini.IniReadValue("COM", "Name") != "")
             {
                 ComPortName.SelectedIndex = ComPortName.FindString(ini.IniReadValue("COM", "Name"));
             } 
             if (ini.IniReadValue("COM", "Speed") != "")
             {
                 ComSpeed.SelectedIndex = ComSpeed.FindString(ini.IniReadValue("COM", "Speed"));
             }

            InitDelegate();

            InitConnectionHTRD();
            flagFirstOpen = false;
            
            // Power and Error
            Power.Image = Properties.Resources.gray;
            Error.Image = Properties.Resources.gray;
        }

        private void ShaperButton_Click(object sender, EventArgs e)
        {
            TabControl.SelectedTab = Shaper;
            panelStart.Visible = false;
            pStateFCh.Visible = true;
            TabControl.Visible = true;

            Comp.Parent = null;
            //Comp.Enabled = false;

           // SpecilFlag = false;

            ini = new IniFile(path + "\\Resources" + "\\SettingsShaper.ini");
            if (ini.IniReadValue("COM", "Name") != "")
            {
                ComPortName.SelectedIndex = ComPortName.FindString(ini.IniReadValue("COM", "Name"));
            }
            if (ini.IniReadValue("COM", "Speed") != "")
            {
                ComSpeed.SelectedIndex = ComSpeed.FindString(ini.IniReadValue("COM", "Speed"));
            }
            
            InitDelegateShaper();
            
            InitConnectionShaper();
            flagFirstOpen = false;
            SendSpecialCommand.Visible = false;
        }

        private void InitDelegateShaper()
        {
            _ShowLogMessage = new dlgShowLogMessage(ShowLogMessageRequest);

        }
        private void InitConnectionShaper()
        {
            try
            {
                if (sLanguage != sRusL)
                    LangBut_Click(null, new EventArgs());
                LangBut.Visible = false;
            }
            catch { }
            if (comShaper != null)
                comShaper = null;
            comShaper = new ComShaper();


            comShaper.OnConnectPort += new ComShaper.ConnectEventHandler(ShowConnectHTRD);
            comShaper.OnDisconnectPort += new ComShaper.ConnectEventHandler(ShowDisconnectHTRD);

            comShaper.OnReceiveCmd += new ComShaper.ByteEventHandler(ShowReceiveRequest);

            // read array of byte
            comShaper.OnReadByte += new ComShaper.ByteEventHandler(ShowReadByteShaper_My);
            //comShaper.OnReceiveCmd += new ComShaper.CmdEventHandler(ShowReceiveRequest);
            // write array of byte
            comShaper.OnWriteByte += new ComShaper.ByteEventHandler(WriteByteShaper);

            if (ComPortName.SelectedIndex >= 0 && ComSpeed.SelectedIndex >= 0)
            {
                comShaper.OpenPort(ComPortName.Items[ComPortName.SelectedIndex].ToString(),
                    Convert.ToInt32(ComSpeed.Items[ComSpeed.SelectedIndex]), Parity.None, 8, StopBits.One);
            }

        }

        private void ShowReadByteShaper_My(byte[] bData)
        {
            if (pbReadHTRD.InvokeRequired)
            {
                pbReadHTRD.Invoke((MethodInvoker)(delegate()
                {
                    pbReadHTRD.Image = Properties.Resources.green;
                    tmReadByteHTRD = new System.Threading.Timer(TimeReadByteHTRD, null, usTimeIndicate, 0);
                }));

            }
            else
            {
                pbReadHTRD.Image = Properties.Resources.green;
                tmReadByteHTRD = new System.Threading.Timer(TimeReadByteHTRD, null, usTimeIndicate, 0);
            }

            if (checkBox2.Checked == true)
            {
                ShowByteShaper(bData);
            }

        }
        private void ShowByteShaper(byte[] bData)
        {

            string mess = "";
            if (bData != null)
            {
                for (int i = 0; i < bData.Length; i++)
                    mess += bData[i].ToString("X2") + " ";
            }
            mess += "\n";
            ShowLogMessageRequest(0, "", mess, "");

        }
        private void WriteByteShaper(byte[] bData)
        {

            if (pbWriteHTRD.InvokeRequired)
            {
                pbWriteHTRD.Invoke((MethodInvoker)(delegate()
                {
                    pbWriteHTRD.Image = Properties.Resources.red;
                    tmWriteByteHTRD = new System.Threading.Timer(TimeWriteByteHTRD, null, usTimeIndicate, 0);
                }));

            }
            else
            {
                pbWriteHTRD.Image = Properties.Resources.red;
                tmWriteByteHTRD = new System.Threading.Timer(TimeWriteByteHTRD, null, usTimeIndicate, 0);
            }

            ShowWtiteShaper(bData);
        }

        private void ShowWtiteShaper(byte[] bByte)
        {

            string hex = BitConverter.ToString(bByte);
            hex = hex.Replace("-", " ");
            hex += "\n";
            ShowLogMessageRequest(7, hex, "", "");
        }

        /*private void ShowReceiveRequest(object structure)
        {
            int sizeInBytes = System.Runtime.InteropServices.Marshal.SizeOf(structure);
            string mess = "";
            string messError = "";
            Answer partAnswer = (Answer)structure;
            switch (partAnswer.mainFiel.cipher)
            {

                case ComShaper.FRCH: case ComShaper.AM: case ComShaper.FM: case ComShaper.CHM: case ComShaper.FielFreq:
                    MiddleRequest request = (MiddleRequest) partAnswer.obj;
                    byte[] crc = BitConverter.GetBytes(request.CRC);
                    mess = partAnswer.mainFiel.Address.ToString("X2") + " " + partAnswer.mainFiel.cipher.ToString("X2") + " " +
                           partAnswer.mainFiel.FieldLength.ToString("X2") + " " + request.Frequency.ToString("X2") + " " +
                           crc[0].ToString("X2") + " " + crc[1].ToString("X2");
                    ShowLogMessageRequest(0, "", mess, "");
                    
                    break;

                case ComShaper.PPRCH:
                    BigRequest Bigrequest = (BigRequest)partAnswer.obj;
                    crc = BitConverter.GetBytes(Bigrequest.CRC);
                    mess = partAnswer.mainFiel.Address.ToString("X2") + " " + partAnswer.mainFiel.cipher.ToString("X2") +
                           " " +
                           partAnswer.mainFiel.FieldLength.ToString("X2") + " " +
                           Bigrequest.StartFrequency.ToString("X2") + " " + Bigrequest.NumberFreq.ToString("X2") + " " +
                           Bigrequest.Step.ToString("X2") + " " + Bigrequest.FreqSaltus.ToString("X2") + " " +
                           crc[0].ToString("X2") + " " + crc[1].ToString("X2");
                    ShowLogMessageRequest(0, "", mess, "");
                    
                    break;

                case ComShaper.OffSignal:case ComShaper.SpecialCipher:
                    SmallRequest smallRequest = (SmallRequest)partAnswer.obj;
                    crc = BitConverter.GetBytes(smallRequest.CRC);
                    mess = partAnswer.mainFiel.Address.ToString("X2") + " " + partAnswer.mainFiel.cipher.ToString("X2") +
                           " " + crc[0].ToString("X2") + " " + crc[1].ToString("X2");
                    ShowLogMessageRequest(0, "", mess, "");
                    
                    break;
            }
            if (partAnswer.mainFiel.cipher == 0xF1)
            {
                Random random = new Random();
                byte T = Convert.ToByte(random.Next(0, 255));
                byte H = Convert.ToByte(random.Next(0, 255));
                byte ADC = Convert.ToByte(random.Next(0, 255));
                comShaper.SendAnswer(partAnswer.mainFiel.cipher, T, H, ADC, 0);
                label6.Text = "Температура = " + T.ToString();
                label7.Text = "Влажность = " + H.ToString();
                label8.Text = "Заряд = " + (ADC_battery(ADC)).ToString();
            }
            else
            {
                Random rand = new Random();
                byte error;
                if (ErroreListBox.SelectedIndex >= 0)
                {
                    error = Convert.ToByte(ErroreListBox.SelectedIndex);
                }
                else
                {
                    error = Convert.ToByte(rand.Next(0, 7));
                }
                comShaper.SendAnswer(partAnswer.mainFiel.cipher, 0, 0, 0, comShaper.listError[error]);
                label9.Text = "Ошибка № " + (error+1).ToString("X2") + " " + comShaper.FindErr(comShaper.listError[error]);
            }
        }
        */

        private void ShowReceiveRequest(byte[] structure)
        {          
            string mess = "";
            if (structure[0] == ComShaper.AddressDispToGen)
            {

                address = ComShaper.AddreddGenToDisp;
            }
            else // structure[0] = ComShaper.AddressToGen
            {
                address = ComShaper.AddressToComp;
            }
            code = structure[1];
            for (int i = 0; i < structure.Length; i++)
            {
                mess += structure[i].ToString("X2") + " ";
            }
            ShowLogMessageRequest(4, mess,"" , "");
            
        }
        // show read/write command message
        private void ShowLogMessageRequest(byte bType, string strTimeCodeAdr, string strInfoMessage, string strTypeCmd)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(_ShowLogMessage, new object[] { bType, strTimeCodeAdr, strInfoMessage, strTypeCmd });
            }
            else
            {
                try
                {
                    if (ShaperBox.TextLength == ShaperBox.MaxLength - 100)
                        ShaperBox.Clear();

                    ShaperBox.AppendText("\n");
                    switch (bType)
                    {
                        case 0:
                            ShaperBox.SelectionColor = Color.Purple;
                            break;

                        case 1:
                            ShaperBox.SelectionColor = Color.Red;
                            break;

                        case 2:
                            ShaperBox.SelectionColor = Color.Blue;
                            break;

                        case 3:
                            ShaperBox.SelectionColor = Color.Blue;
                            break;

                        case 4:
                            ShaperBox.SelectionColor = Color.Red;
                            break;

                        default:
                            ShaperBox.SelectionColor = Color.Black;
                            break;
                    }

                    ShaperBox.AppendText(strTimeCodeAdr);
                    ShaperBox.SelectionColor = Color.Black;
                    ShaperBox.AppendText(strTypeCmd);
                    ShaperBox.SelectionColor = Color.Black;
                    ShaperBox.AppendText(strInfoMessage);
                    ShaperBox.SelectionStart = ShaperBox.TextLength;
                    ShaperBox.ScrollToCaret();
                }
                catch (System.Exception ex)
                {
                    //GlobalVar._LogFile.LogWrite("   " + this.Name + ":  " + ex.TargetSite + "   " + ex.Message); 
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error,
                        MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                }
                TimerShaper.Enabled = true;
            }
        }

        private void StartFreq_TextChanged(object sender, EventArgs e)
        {
            StartFreq.BackColor = Color.Aquamarine;
        }

        private void NumFreqBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((!char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                e.Handled = true;
            }
        }

        private void StepPPRCHBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((!char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                e.Handled = true;
            }
        }

        private void LeapBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((!char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                e.Handled = true;
            }
        }

        private void StepBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((!char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
        }

        private void StartFreq_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((!char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
        }

        private void OffButton_Click(object sender, EventArgs e)
        {
            Invoke((MethodInvoker)(() =>
                {
                    if (tmAutoEnter != null)
                        tmAutoEnter.Dispose();
                }));
            CountMaxEnter = 0;
            timerError.Enabled = false;
            timerEnabled.Enabled = false;
            ShowStateButton(bSendALX, true);
            ShowStateButton(OnButton, true);
            ShowStateButton(OffButton, true);

            flagOFF = true;
            try
            {
                ErrorLabel.Text = TranslateDic[ErrorLabel.Name];
                sCodeMessFromShaper = "";
            }
            catch { ErrorLabel.Text = "Сообщение:  "; }
            if (comHTRD != null)
                SpecialSend(0, ComHTRD.OffSignal, 0, 0, 0, 0); //comHTRD.Send(0, ComHTRD.OffSignal, 0, 0, 0, 0);

        }

        private void StepBox_TextChanged(object sender, EventArgs e)
        {
            //StepBox.BackColor = Color.Aquamarine;
        }
        
        private void Plus_Click(object sender, EventArgs e)
        {
            if (comHTRD != null)
            {
                if (StepBox.Text != "")
                {
                    try
                    {
                        if (Convert.ToDouble(StepBox.Text) >= MinFreq)
                        {
                            StepBox.BackColor = Color.White;
                            if (StepBox.Text != "" && StepBox.BackColor == Color.White)
                            {
                                try
                                {
                                    if (StartFreq.Text == "")
                                    {
                                        StartFreq.Text = 0.ToString();
                                    }
                                    double buff = Convert.ToDouble(StartFreq.Text) + Convert.ToDouble(StepBox.Text);
                                    if (buff >= MinFreq && buff <= MaxFreq)
                                    {
                                        StartFreq.Text = buff.ToString("####.000");
                                    }
                                    else // buff>MaxFreq
                                    {
                                        StartFreq.Text = MaxFreq.ToString("####.000");
                                    }
                                }
                                catch (Exception)
                                {
                                    MessageBox.Show("Начальная частота задана неверно");
                                    StartFreq.Clear();
                                    StartFreq.Focus();
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Введите значение шага перестройки");
                        StepBox.Clear();
                        StepBox.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("Введите значение шага перестройки");
                    StepBox.Clear();
                    StepBox.Focus();
                }
                
            }
        }

        private void Minus_Click(object sender, EventArgs e)
        {
            if (comHTRD != null)
            {
                if (StepBox.Text != "")
                {
                    try
                    {
                        if (Convert.ToDouble(StepBox.Text) >= MinFreq)
                            ///&& (Convert.ToInt32(StepBox.Text) <= (Convert.ToInt32(StartFreq.Text) / 2)))
                        {
                            try
                            {
                                StepBox.BackColor = Color.White;
                                if (StartFreq.Text == "")
                                {
                                    StartFreq.Text = MaxFreq.ToString();
                                }
                                double buff = Convert.ToDouble(StartFreq.Text) - Convert.ToDouble(StepBox.Text);
                                if (buff >= MinFreq && buff <= MaxFreq)
                                {
                                    StartFreq.Text = buff.ToString("####.000");
                                }
                                else // buff<MinFreq
                                {
                                    StartFreq.Text = MinFreq.ToString("####.000");
                                }
                            }
                            catch (Exception)
                            {
                                MessageBox.Show("Начальная частота задана неверно");
                                StartFreq.Clear();
                                StartFreq.Focus();
                            }
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Введите значение шага перестройки");
                        StepBox.Clear();
                        StepBox.Focus();
                    }

                }
                else
                {
                    MessageBox.Show("Введите значение шага перестройки");
                    StepBox.Clear();
                    StepBox.Focus();
                }

            }
        }

        private void OnButton_Click(object sender, EventArgs e)
        {
            try
            {
                ErrorLabel.Text = TranslateDic[ErrorLabel.Name];
                sCodeMessFromShaper = "";
            }
            catch { ErrorLabel.Text = "Сообщение:  "; }
            Power.Visible = true;
            Power.Image = Properties.Resources.gray;
            if (comHTRD != null && comHTRD.PortConnect())
            {
                switch (SelectedTabName)//(CodeName.SelectedTab.Name)
                {
                    case "OtherCode":
                        if (StartFreq.Text != "")
                        {
                            try
                            {
                                if (Convert.ToDouble(StartFreq.Text) < MinFreq)
                                    StartFreq.Text = MinFreq.ToString();
                                if (Convert.ToDouble(StartFreq.Text) > MaxFreq)
                                    StartFreq.Text = MaxFreq.ToString();
                                if (Convert.ToDouble(StartFreq.Text) >= MinFreq &&
                                    Convert.ToDouble(StartFreq.Text) <= MaxFreq)
                                {
                                    StartFreq.BackColor = Color.White;
                                    if (TypeFreqBox.SelectedIndex >= 0)
                                    {
                                        Int32 Freq = (Int32)(Convert.ToDouble(StartFreq.Text) * 1000);
                                        SpecialSend(Freq,comHTRD.ListCodegram[TypeFreqBox.SelectedIndex], 0, 0, 0, Convert.ToByte(Duration.Value));
                                        //comHTRD.Send(Freq,comHTRD.ListCodegram[checkedListBox1.SelectedIndex], 0, 0, 0, Convert.ToByte(Duration.Value));
                                    }
                                    else
                                    {
                                        MessageBox.Show("Необходимо выбрать кодограмму");
                                        TypeFreqBox.Focus();
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Ошибка ввода начальной частоты");
                                    StartFreq.Clear();
                                    StartFreq.Focus();
                                }
                            }
                            catch (Exception)
                            {
                                MessageBox.Show("Ошибка ввода начальной частоты");
                                StartFreq.Clear();
                                StartFreq.Focus();
                            }



                        }
                        else
                        {
                            MessageBox.Show("Введите значение частоты");
                            StartFreq.Focus();
                        }
                        break;
                    case "PPRCHcode":
                        if (NumFreq.SelectedIndex >= 0)
                        {
                            if (SaltusBox.Text != "") 
                            {
                                if(Convert.ToInt16(SaltusBox.Text) < MinSaltusPprch)
                                    SaltusBox.Text = MinSaltusPprch.ToString();
                                if (Convert.ToInt16(SaltusBox.Text) > MaxSaltusPprch)
                                    SaltusBox.Text = MaxSaltusPprch.ToString();

                                SaltusBox.BackColor = Color.White;
                                if (StepPPRCHBox.Text != "")
                                {
                                    if (Convert.ToInt16(StepPPRCHBox.Text) > MaxStepPprch)
                                        StepPPRCHBox.Text = MaxStepPprch.ToString();
                                    if (Convert.ToInt16(StepPPRCHBox.Text) < MinStepPprch)
                                        StepPPRCHBox.Text = MinStepPprch.ToString();

                                    StepPPRCHBox.Text =
                                        (Math.Round(Convert.ToInt16(StepPPRCHBox.Text) / 25.0) * 25).ToString();
                                    StepPPRCHBox.BackColor = Color.White;
                                    try
                                    {
                                        if (StartFreqPPRCH.Text != "" && Convert.ToDouble(StartFreqPPRCH.Text) >= MinFreq &&
                                            Convert.ToDouble(StartFreqPPRCH.Text) <= MaxFreq)
                                        {
                                            StartFreqPPRCH.BackColor = Color.White;
                                            Int32 Freq = (Int32)(Convert.ToDouble(StartFreqPPRCH.Text) * 1000);


                                            // comment 13.09.2019 (ask OV)
                                            //SpecialSend(Freq, ComHTRD.PPRCH, 
                                            //    NumberFreq[NumFreq.SelectedIndex], 
                                            //    Convert.ToByte(Math.Round(Convert.ToInt16(StepPPRCHBox.Text) / 25.0)), 
                                            //    Convert.ToInt16(SaltusBox.Text), Convert.ToByte(Duration.Value));


                                            SpecialSend(Freq, ComHTRD.PPRCH,
                                                    Convert.ToByte(NumFreq.Items[NumFreq.SelectedIndex]),
                                                    Convert.ToByte(Math.Round(Convert.ToInt16(StepPPRCHBox.Text) / 25.0)),
                                                    Convert.ToInt16(SaltusBox.Text), Convert.ToByte(Duration.Value));

                                            // comHTRD.Send(Freq, ComHTRD.PPRCH, NumberFreq[NumFreq.SelectedIndex], Convert.ToByte(Math.Round(Convert.ToByte(StepPPRCHBox.Text) / 25.0)), Convert.ToInt16(SaltusBox.Text), Convert.ToByte(Duration.Value));
                                        }
                                        else
                                        {
                                            MessageBox.Show("Введите значение частоты");
                                            StartFreq.Focus();
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        MessageBox.Show("Ошибка ввода начальной частоты");
                                        StartFreq.Clear();
                                        StartFreq.Focus();
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Введите значение шага перестройки");
                                    StepPPRCHBox.Focus();
                                }
                            }
                            else
                            {
                                SaltusBox.Clear();
                                MessageBox.Show("Введите значение частоты скачков");
                                SaltusBox.Focus();
                            }
                        }
                        else
                        {
                            MessageBox.Show("Необходимо выбрать количество частот");
                            NumFreq.Focus();
                        }
                        break;
                    case "FileFreqCode":
                        if (StepFileFreq.SelectedIndex >= 0)
                        {
                                try
                                {
                                    string[] SelectStrip = NumberStrip[Convert.ToInt16(EPO.Value) - 1].Split('-');
                                        Int32 Freq = (int)((Convert.ToInt32(SelectStrip[1]) + Convert.ToInt32(SelectStrip[0]))/2)*1000;
                                        if (CheckBoxShift.Checked == true)
                                            Freq -= (1000 * ArrayStepFreq[StepFileFreq.SelectedIndex] / 2);
                                        SpecialSend(Freq, ComHTRD.FielFreq, 
                                            Convert.ToByte(NumberFileFreq), ArrayStepFreq[StepFileFreq.SelectedIndex], 0, Convert.ToByte(Duration.Value));
                                        //comHTRD.Send(Freq, ComHTRD.FielFreq, Convert.ToByte(NumberFileFreq), ArrayStepFreq[StepFileFreq.SelectedIndex], 0, Convert.ToByte(Duration.Value));
                                    
                                }
                                catch (Exception)
                                {
                                    MessageBox.Show("Ошибка ввода начальной частоты");
                                    StartFreq.Clear();
                                    StartFreq.Focus();
                                }                           
                        }
                        else
                        {
                            MessageBox.Show("Необходимо выбрать шаг перестройки по частоте!");
                            StepFileFreq.Focus();
                        }
                        break;

                    case "PollCode":
                        if (NumBoxStartFreq.Value >= NumBoxStopFreq.Value) 
                        {
                            MessageBox.Show("Частота СТОП должна быть Больше частоты СТАРТ");
                            return;
                        }
                        CurrentFreq = (int)NumBoxStartFreq.Value * 1000;
                        CurrentStep = (int)NumBoxStepAutoReq.Value * 1000;
                        //LabCurrentFreq.Text = "Текущая частота   " + (CurrentFreq / 1000).ToString();
                        byte time;
                        try { time = Convert.ToByte(ini.IniReadValue("Poll", "Duration")); }
                        catch { time = 5; }
                        SpecialSend(CurrentFreq, ComHTRD.FRCH, 0, 0, 0, time);
                        break;


                }
            }
        }
        void FreqPoll(byte error) // функция автоматического опроса
        {
            if(SelectedTabName == PollCode.Name && error == 0x00) //(CodeName.SelectedTab == PollCode && error == 0x00)
            {                
                CurrentFreq = CurrentFreq + CurrentStep;
                if (CurrentFreq > (int)NumBoxStopFreq.Value * 1000)
                    return;
               // Invoke((MethodInvoker)(() => LabCurrentFreq.Text = "Текущая частота   " + (CurrentFreq / 1000).ToString()));
                Invoke((MethodInvoker)(() => NumBoxStartFreq.Value = (CurrentFreq / 1000)));
                byte time;
                try { time = Convert.ToByte(ini.IniReadValue("Poll", "Duration")); }
                catch { time = 5; }
                SpecialSend(CurrentFreq, ComHTRD.FRCH, 0, 0, 0, time);

            }
        }
        private void checkedListBox1_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            var list = sender as CheckedListBox;
            if (e.NewValue == CheckState.Checked)
                foreach (int index in list.CheckedIndices)
                    if (index != e.Index)
                        list.SetItemChecked(index, false);
        }

        private void Comp_Click(object sender, EventArgs e)
        {

        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            rtbLog.Clear();
        }


        private void StepPPRCHBox_TextChanged(object sender, EventArgs e)
        {
            StepPPRCHBox.BackColor = Color.Aquamarine;
        }


        private void StepPPRCHBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (comHTRD != null)
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (StepPPRCHBox.Text != "")
                    {
                        if (Convert.ToInt16(StepPPRCHBox.Text) >= MinStepPprch &&
                            Convert.ToInt16(StepPPRCHBox.Text) <= MaxStepPprch)
                        {
                            StepPPRCHBox.Text = (Math.Round(Convert.ToInt16(StepPPRCHBox.Text) / 25.0) * 25).ToString();
                            StepPPRCHBox.BackColor = Color.White;
                        }
                        else
                        {
                            StepPPRCHBox.Clear();
                        }
                    }

                }
            }
        }

        private void SaltusBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((!char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                e.Handled = true;
            }
        }

        private void SaltusBox_TextChanged(object sender, EventArgs e)
        {
            SaltusBox.BackColor = Color.Aquamarine;
        }

        private void SaltusBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (comHTRD != null)
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (Convert.ToInt16(SaltusBox.Text) >= MinSaltusPprch && Convert.ToInt16(SaltusBox.Text) <= MaxSaltusPprch)
                    {
                        SaltusBox.BackColor = Color.White;
                    }
                    else
                    {
                        SaltusBox.Clear();
                    }

                }
            }

        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            ShaperBox.Clear();
        }

        private void ComSpeed_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (flagFirstOpen == false)
            {
                if (bChannelHTRD.BackColor == Color.Red)
                {
                    if (TabControl.SelectedTab == Comp)
                    {
                        InitConnectionHTRD();
                    }
                    else
                    {
                        InitConnectionShaper();
                    }
                }
                else
                {
                    if (comHTRD != null)
                    {

                        comHTRD.ClosePort();
                        InitConnectionHTRD();
                    }
                    if (comShaper != null)
                    {
                        comShaper.ClosePort();
                        InitConnectionShaper();
                    }
                }

               /* if (TabControl.SelectedTab == Comp)
                {
                    InitConnectionHTRD();
                }
                else // TabControl.SelectedTab = Shaper
                {
                    InitConnectionShaper();
                }*/
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                ini.IniWriteValue("Parameters", "Language ", sLanguage == sRusL ? "0" : "1");
            }
            catch { }
        }

        private void ErroreListBox_ItemCheck(object sender, ItemCheckEventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            TimerShaper.Interval = (int)numericUpDown1.Value;
        }

        private void TimerShaper_Tick(object sender, EventArgs e)
        {
            if (comShaper != null)
            {
                if (code == 0xF1)
                {

                    Random random = new Random();
                    byte T = Convert.ToByte(random.Next(0, 255));
                    byte H = Convert.ToByte(random.Next(0, 255));
                    byte ADC = Convert.ToByte(random.Next(118, 155));

                    comShaper.SendAnswer(code, T, H, ADC, 0, address);
                    label6.Text = "Температура = " + T.ToString() + " " + "\u00B0" + "C";
                    label7.Text = "Влажность = " + H.ToString() + " " + "\u0025";
                    label8.Text = "Заряд = " + (ADC_battery(ADC)).ToString() + " " + "B";
                }
                else
                {

                    Random rand = new Random();
                    byte error;
                    if (ErroreListBox.SelectedIndex >= 0)
                    {
                       /* switch (ErroreListBox.SelectedIndex)
                        {
                            case 6:
                                error = 0x0D;
                                break;
                            case 7:
                                error = 0x10;
                                break;
                            case 8:
                                error = 0x11;
                                break;
                            default:
                                error = Convert.ToByte(ErroreListBox.SelectedIndex);
                                break;
                        }*/
                        error = Convert.ToByte(ErroreListBox.SelectedIndex);
                    }
                    else
                    {
                        error = Convert.ToByte(rand.Next(0, 9));
                    }
                    comShaper.SendAnswer(code, 0, 0, 0, comShaper.listError[error], address);
                    //label9.Text = "Сообщение: " + (error + 1).ToString("X2") + " " + comShaper.FindErr(comShaper.listError[error]);

                    label9.Text = "Сообщение: " + " " + comShaper.FindErr(comShaper.listError[error]);
                }
                TimerShaper.Enabled = false;
            }
        }

        private void numericUpDown1_KeyPress(object sender, KeyPressEventArgs e)
        {

            if ((!char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                e.Handled = true;
            }
        }

        private void numericUpDown1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                TimerShaper.Interval = (int) numericUpDown1.Value;
            }
        }

        //private int remains = 300000; //300 second
        private void timerError_Tick(object sender, EventArgs e)
        {/*
            if (ErrorLabel.InvokeRequired)
            {
                ErrorLabel.Invoke((MethodInvoker)(delegate()
                {
                    ErrorLabel.Text = "Ошибка радиоканала!";
                }));

            }
            else
            {
                ErrorLabel.Text = "Ошибка радиоканала!";
            }
            if (Error.InvokeRequired)
            {
                Error.Invoke((MethodInvoker)(delegate()
                {
                    Error.Image = Properties.Resources.red;
                }));

            }
            else
            {
                Error.Image = Properties.Resources.red;
            }
            timerError.Enabled = false;*/
        }

        private void timerOpen_Tick(object sender, EventArgs e)
        {
            string[] portnames = SerialPort.GetPortNames();
            int index = Array.IndexOf(portnames, ComPortName.Items[ComPortName.SelectedIndex]);
            if ( index < 0)
            {
                ShowDisconnectHTRD();
                timerOpen.Enabled = false;
            }

        }

        private void timerEnabled_Tick(object sender, EventArgs e)
        {
            ShowStateButton(bSendALX, true);
            timerEnabled.Enabled = false;
        }

        private void PlusPPRCH_Click(object sender, EventArgs e)
        {
            if (comHTRD != null)
            {
                if (StepStartFreq.Text != "")
                {
                    try
                    {
                        if (Convert.ToDouble(StepStartFreq.Text) >= MinFreq)
                        {
                            StepStartFreq.BackColor = Color.White;
                            if (StepStartFreq.Text != "" && StepStartFreq.BackColor == Color.White)
                            {
                                try
                                {
                                    if (StartFreqPPRCH.Text == "")
                                    {
                                        StepStartFreq.Text = 0.ToString();
                                    }
                                    double buff = Convert.ToDouble(StartFreqPPRCH.Text) + Convert.ToDouble(StepStartFreq.Text);
                                    if (buff >= MinFreq && buff <= MaxFreq)
                                    {
                                        StartFreqPPRCH.Text = buff.ToString("####.000");
                                    }
                                    else // buff>MaxFreq
                                    {
                                        StartFreqPPRCH.Text = MaxFreq.ToString("####.000");
                                    }
                                }
                                catch (Exception)
                                {
                                    MessageBox.Show("Начальная частота задана неверно");
                                    StartFreqPPRCH.Clear();
                                    StartFreqPPRCH.Focus();
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Введите значение шага перестройки");
                        StepStartFreq.Clear();
                        StepStartFreq.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("Введите значение шага перестройки");
                    StepStartFreq.Clear();
                    StepStartFreq.Focus();
                }

            }
        }

        private void MinusPPRCH_Click(object sender, EventArgs e)
        {
            if (comHTRD != null)
            {
                if (StepStartFreq.Text != "")
                {
                    try
                    {
                        if (Convert.ToDouble(StepStartFreq.Text) >= MinFreq)
                        ///&& (Convert.ToInt32(StepBox.Text) <= (Convert.ToInt32(StartFreq.Text) / 2)))
                        {
                            try
                            {
                                StepStartFreq.BackColor = Color.White;
                                if (StartFreqPPRCH.Text == "")
                                {
                                    StartFreqPPRCH.Text = MaxFreq.ToString();
                                }
                                double buff = Convert.ToDouble(StartFreqPPRCH.Text) - Convert.ToDouble(StepStartFreq.Text);
                                if (buff >= MinFreq && buff <= MaxFreq)
                                {
                                    StartFreqPPRCH.Text = buff.ToString("####.000");
                                }
                                else // buff<MinFreq
                                {
                                    StartFreqPPRCH.Text = MinFreq.ToString("####.000");
                                }
                            }
                            catch (Exception)
                            {
                                MessageBox.Show("Начальная частота задана неверно");
                                StartFreqPPRCH.Clear();
                                StartFreqPPRCH.Focus();
                            }
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Введите значение шага перестройки");
                        StepStartFreq.Clear();
                        StepStartFreq.Focus();
                    }

                }
                else
                {
                    MessageBox.Show("Введите значение шага перестройки");
                    StepStartFreq.Clear();
                    StepStartFreq.Focus();
                }

            }
        }

        private void StartFreqPPRCH_TextChanged(object sender, EventArgs e)
        {
            StartFreqPPRCH.BackColor = Color.Aquamarine;
        }

        private void EPO_ValueChanged(object sender, EventArgs e)
        {
            label_NumStrip.Text = NumberStrip[Convert.ToInt16(EPO.Value) - 1];
                       
            ChangeNumberFileFreq();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try { EPO.Value += 1; }
            catch { }
            ChangeNumberFileFreq();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try { EPO.Value -= 1; }
            catch { }
            ChangeNumberFileFreq();
        }

        private void StepFileFreq_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChangeNumberFileFreq();
        }

        public void ChangeNumberFileFreq()
        {
            try
            {
                int StepFreq;
                switch (StepFileFreq.SelectedIndex)
                {
                    case 0:
                        StepFreq = 1;
                        break;
                    case 1:
                        StepFreq = 2;
                        break;
                    case 2:
                        StepFreq = 5;
                        break;
                    case 3:
                        StepFreq = 15;
                        break;
                    default:
                        StepFreq = -1;
                        break;
                }
                if (StepFreq == -1)
                {
                    //MessageBox.Show("Необходимо выбрать шаг перестройки по частоте!");
                    //StepFileFreq.Focus();
                }
                else
                {
                    //в зависимости от выбранного диапазона и шага перестройки рассчитываем количество частот
                    string[] SelectStrip = NumberStrip[Convert.ToInt16(EPO.Value) - 1].Split('-');
                    if (SelectStrip.Length == 2)
                    {
                        NumberFileFreq = (int)((Convert.ToInt32(SelectStrip[1]) - Convert.ToInt32(SelectStrip[0])) / StepFreq);
                        if (EPO.Value == NumberStrip.Length) 
                        {
                            NumberFileFreq += 1; // Добавляем крайнюю частоту в последнем диапазоне
                        } 
                        NuberFileFreqLabel.Text = "Количество частот: " + NumberFileFreq.ToString();
                    }
                    else
                    {
                        MessageBox.Show("Необходимо проверить файл  диапазона полос!");
                    }
                }
            }
            catch { }
        }

        private void ErroreListBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void FileFreq_Click(object sender, EventArgs e)
        {

        }

        private void ErroreListBox_ItemCheck_1(object sender, ItemCheckEventArgs e)
        {
            var list = sender as CheckedListBox;
            if (e.NewValue == CheckState.Checked)
                foreach (int index in list.CheckedIndices)
                    if (index != e.Index)
                        list.SetItemChecked(index, false);
        }

        private void SendSpecialCommand_Click(object sender, EventArgs e)
        {
            if (SendSpecialCommand.BackColor == Color.LightGray)
            {
                tmrAutoSendALX.Enabled = true;
                SendSpecialCommand.BackColor = Color.Aquamarine;
            }
            else
            {
                tmrAutoSendALX.Enabled = false;
                SendSpecialCommand.BackColor = Color.LightGray;
            }
        }

        private void LangBut_Click(object sender, EventArgs e)
        {
            if (LangBut.Text.CompareTo("Ru") == 0)
            {
                
                LangBut.Text = (iLangugeSwitch == 0 ? "Az" : "EN") ;
                sLanguage = (iLangugeSwitch == 0 ? sAzL : sEng);
                ChangaLanguage();
            }
            else
            {
                LangBut.Text = "Ru";
                sLanguage = sRusL;
                ChangaLanguage();
            }
        }
        void LoadDictionary()
        {
            XmlDocument xDoc = new XmlDocument();
            if (System.IO.File.Exists(path + "\\Resources" + "\\Translation.xml"))
                xDoc.Load(path + "\\Resources" + "\\Translation.xml");
            else
            {
                switch (sLanguage)
                {

                    case sRusL:
                        MessageBox.Show("Файл XMLTranslation.xml не найден!", "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case sEng:
                        MessageBox.Show("File XMLTranslation.xml not found!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case sAzL:
                        MessageBox.Show("Fayl XMLTranslation.xml tapılmadı!", "Səhv!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                }

                return;
            }
            TranslateDic = new Dictionary<string, string>();
            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlNode x2Node in xRoot.ChildNodes)
            {
                if (x2Node.NodeType == XmlNodeType.Comment)
                    continue;

                // получаем атрибут ID
                if (x2Node.Attributes.Count > 0)
                {
                    XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
                    if (attr != null)
                    {
                        foreach (XmlNode childnode in x2Node.ChildNodes)
                        {
                            // если узел - language
                            if (childnode.Name == sLanguage)
                            {
                                TranslateDic.Add(attr.Value, childnode.InnerText);
                            }

                        }
                    }
                }
            }
        }
        void ChangaLanguage()
        {
            LoadDictionary();
            List<Control.ControlCollection> AllControl = new List<Control.ControlCollection>();
            AllControl.Add(pButton.Controls);
            AllControl.Add(MainPanel.Controls);
            AllControl.Add(Comp.Controls);
            AllControl.Add(OtherCode.Controls);
            AllControl.Add(PPRCHcode.Controls);
            AllControl.Add(ErrorPanel.Controls);
            AllControl.Add(CodeName.Controls);
            AllControl.Add(TabControl.Controls);
            ForeachControl(AllControl);
            string message = "";
            if (sCodeMessFromShaper != "")
            {
                string messError = TranslateDic[sCodeMessFromShaper];
                message = TranslateDic[ErrorLabel.Name] + messError; // "Сообщение:  " + messError;
                ShowInfoLabel(ErrorLabel, message);
            }
            if (sTemp != "")
            {
                message = TranslateDic[Templabel.Name] + sTemp;
                ShowInfoLabel(Templabel, message);
            }
            if (sHumidid != "")
            {
                message = TranslateDic[Humididylabel.Name] + sHumidid;
                ShowInfoLabel(Humididylabel, message);
            }
            if (sCharge != "")
            {
                message = TranslateDic[Chargelabel.Name] + sCharge;
                ShowInfoLabel(Chargelabel, message);
            }
            /*
            ForeachControl(pButton.Controls);
            ForeachControl(MainPanel.Controls);
            ForeachControl(Comp.Controls);
            ForeachControl(OtherCode.Controls);
            ForeachControl(PPRCHcode.Controls);
            ForeachControl(ErrorPanel.Controls);
            ForeachControl(CodeName.Controls);
            ForeachControl(TabControl.Controls);*/
        }

        void ForeachControl(Control.ControlCollection control)
        {
            foreach (Control value in control)
            {
                string type = value.GetType().ToString();
                string name = "";
                switch (type)
                {
                    case "System.Windows.Forms.Button":
                        Button CurrentButton = value as Button;
                        if (CurrentButton != null)
                        {
                            name = (value as Button).Name;
                            foreach (string fi in TranslateDic.Keys)
                                if (fi == name)
                                {
                                    (value as Button).Text = TranslateDic[fi];
                                }
                        }
                        break;
                    case "System.Windows.Forms.Label":
                        name = "";
                        Label CurrentLabel = value as Label;
                        if (CurrentLabel != null)
                        {
                            name = (value as Label).Name;
                            foreach (string fi in TranslateDic.Keys)
                                if (fi == name)
                                {
                                    (value as Label).Text = TranslateDic[fi];
                                }
                        }
                        break;
                    case "System.Windows.Forms.CheckedListBox":
                        name = "";
                        CheckedListBox CurrentCheckBox = value as CheckedListBox;
                        if (CurrentCheckBox != null)
                        {
                            for (int i = 0; i < CurrentCheckBox.Items.Count; i++)
                            {
                                name = "TypeFreqBox_" + i;
                                foreach (string fi in TranslateDic.Keys)
                                    if (fi == name)
                                    {
                                        (value as CheckedListBox).Items[i] = TranslateDic[fi];
                                    }
                            }
                        }
                        break;
                    case "System.Windows.Forms.CheckBox":
                        name = "";
                        CheckBox checkBox = value as CheckBox;
                        if (checkBox != null)
                        {
                            name = (value as CheckBox).Name;
                            foreach (string fi in TranslateDic.Keys)
                                if (fi == name)
                                {
                                    (value as CheckBox).Text = TranslateDic[fi];
                                }
                        }
                        break;
                    case "System.Windows.Forms.TabPage":
                        name = "";
                        TabPage tabPage = value as TabPage;
                        if (tabPage != null)
                        {
                            name = (value as TabPage).Name;
                            foreach (string fi in TranslateDic.Keys)
                                if (fi == name)
                                {
                                    (value as TabPage).Text = TranslateDic[fi];
                                }
                        }
                        break;
                }
            }
 
        }

        void ForeachControl(List<Control.ControlCollection> ListControl)
        {
            foreach (Control.ControlCollection controlCollection in ListControl)
                foreach (Control value in controlCollection)
                {
                    string type = value.GetType().ToString();
                    string name = "";
                    switch (type)
                    {
                        case "System.Windows.Forms.Button":
                            Button CurrentButton = value as Button;
                            if (CurrentButton != null)
                            {
                                name = (value as Button).Name;
                                foreach (string fi in TranslateDic.Keys)
                                    if (fi == name)
                                    {
                                        (value as Button).Text = TranslateDic[fi];
                                    }
                            }
                            break;
                        case "System.Windows.Forms.Label":
                            name = "";
                            Label CurrentLabel = value as Label;
                            if (CurrentLabel != null)
                            {
                                name = (value as Label).Name;
                                foreach (string fi in TranslateDic.Keys)
                                    if (fi == name)
                                    {
                                        (value as Label).Text = TranslateDic[fi];
                                        (value as Label).TextAlign = ContentAlignment.MiddleLeft;
                                    }
                            }
                            break;
                        case "System.Windows.Forms.CheckedListBox":
                            name = "";
                            CheckedListBox CurrentCheckBox = value as CheckedListBox;
                            if (CurrentCheckBox != null)
                            {
                                for (int i = 0; i < CurrentCheckBox.Items.Count; i++)
                                {
                                    name = "TypeFreqBox_" + i;
                                    foreach (string fi in TranslateDic.Keys)
                                        if (fi == name)
                                        {
                                            (value as CheckedListBox).Items[i] = TranslateDic[fi];
                                        }
                                }
                            }
                            break;
                        case "System.Windows.Forms.CheckBox":
                            name = "";
                            CheckBox checkBox = value as CheckBox;
                            if (checkBox != null)
                            {
                                name = (value as CheckBox).Name;
                                foreach (string fi in TranslateDic.Keys)
                                    if (fi == name)
                                    {
                                        (value as CheckBox).Text = TranslateDic[fi];
                                    }
                            }
                            break;
                        case "System.Windows.Forms.TabPage":
                            name = "";
                            TabPage tabPage = value as TabPage;
                            if (tabPage != null)
                            {
                                name = (value as TabPage).Name;
                                foreach (string fi in TranslateDic.Keys)
                                    if (fi == name)
                                    {
                                        (value as TabPage).Text = TranslateDic[fi];
                                    }
                            }
                            break;
                    }
                }

        }

        private void CodeName_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectedTabName = CodeName.SelectedTab.Name;
            if (CodeName.SelectedTab == PollCode)
                usTimeDelay = PollTimeDelay;
            else
                usTimeDelay = 0;
        }

        private void StepStartFreq_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int valStep = Convert.ToInt32(StepStartFreq.Text);
                if (valStep  == 1)
                    NumFreq.Items[6] = "121";
                else
                    NumFreq.Items[6] = "128";
            }
            catch
            {
                NumFreq.Items[6] = "121";
            }
        }
    }
}

