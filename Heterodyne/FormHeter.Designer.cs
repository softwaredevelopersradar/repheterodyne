﻿namespace Heterodyne
{
    partial class FormHeter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormHeter));
            this.pStateFCh = new System.Windows.Forms.Panel();
            this.LangBut = new System.Windows.Forms.Button();
            this.SendSpecialCommand = new System.Windows.Forms.Button();
            this.ComSpeed = new System.Windows.Forms.ComboBox();
            this.UpDate = new System.Windows.Forms.Button();
            this.ComPortName = new System.Windows.Forms.ComboBox();
            this.pIndicateRCh = new System.Windows.Forms.Panel();
            this.pbReadHTRD = new System.Windows.Forms.PictureBox();
            this.pbWriteHTRD = new System.Windows.Forms.PictureBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lReceiverChannel = new System.Windows.Forms.Label();
            this.bChannelHTRD = new System.Windows.Forms.Button();
            this.tmrAutoSendALX = new System.Windows.Forms.Timer(this.components);
            this.TabControl = new System.Windows.Forms.TabControl();
            this.Comp = new System.Windows.Forms.TabPage();
            this.AllBytesBox = new System.Windows.Forms.CheckBox();
            this.ErrorPanel = new System.Windows.Forms.Panel();
            this.ErrorLabel = new System.Windows.Forms.Label();
            this.ClearButCom = new System.Windows.Forms.Button();
            this.pButton = new System.Windows.Forms.Panel();
            this.DurationLab = new System.Windows.Forms.Label();
            this.Duration = new System.Windows.Forms.NumericUpDown();
            this.ErrorLab = new System.Windows.Forms.Label();
            this.PowerLab = new System.Windows.Forms.Label();
            this.Power = new System.Windows.Forms.PictureBox();
            this.Error = new System.Windows.Forms.PictureBox();
            this.OffButton = new System.Windows.Forms.Button();
            this.OnButton = new System.Windows.Forms.Button();
            this.CodeName = new System.Windows.Forms.TabControl();
            this.OtherCode = new System.Windows.Forms.TabPage();
            this.TypeFreqBox = new System.Windows.Forms.CheckedListBox();
            this.FreqLabOther = new System.Windows.Forms.Label();
            this.StartFreq = new System.Windows.Forms.TextBox();
            this.Plus = new System.Windows.Forms.Button();
            this.StepBox = new System.Windows.Forms.TextBox();
            this.Minus = new System.Windows.Forms.Button();
            this.StepLabOther = new System.Windows.Forms.Label();
            this.PPRCHcode = new System.Windows.Forms.TabPage();
            this.StepStartFreq = new System.Windows.Forms.TextBox();
            this.StepLabPPRCH = new System.Windows.Forms.Label();
            this.FreqLabPPRCH = new System.Windows.Forms.Label();
            this.StartFreqPPRCH = new System.Windows.Forms.TextBox();
            this.PlusPPRCH = new System.Windows.Forms.Button();
            this.MinusPPRCH = new System.Windows.Forms.Button();
            this.NumFreq = new System.Windows.Forms.ComboBox();
            this.SaltusBox = new System.Windows.Forms.TextBox();
            this.FregOfJampLab = new System.Windows.Forms.Label();
            this.StepPPRCHBox = new System.Windows.Forms.TextBox();
            this.StepTuningLab = new System.Windows.Forms.Label();
            this.NumFreqLab = new System.Windows.Forms.Label();
            this.FileFreqCode = new System.Windows.Forms.TabPage();
            this.CheckBoxShift = new System.Windows.Forms.CheckBox();
            this.label_NumStrip = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.StepFileFreq = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.NuberFileFreqLabel = new System.Windows.Forms.Label();
            this.EPO = new System.Windows.Forms.NumericUpDown();
            this.PollCode = new System.Windows.Forms.TabPage();
            this.LabCurrentFreq = new System.Windows.Forms.Label();
            this.NumBoxStopFreq = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.NumBoxStartFreq = new System.Windows.Forms.NumericUpDown();
            this.NumBoxStepAutoReq = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.LabStepAutoReq = new System.Windows.Forms.Label();
            this.bSendALX = new System.Windows.Forms.Button();
            this.rtbLog = new System.Windows.Forms.RichTextBox();
            this.MainPanel = new System.Windows.Forms.Panel();
            this.Chargelabel = new System.Windows.Forms.Label();
            this.Humididylabel = new System.Windows.Forms.Label();
            this.Templabel = new System.Windows.Forms.Label();
            this.Shaper = new System.Windows.Forms.TabPage();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.ErroreListBox = new System.Windows.Forms.CheckedListBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.ClearButShaper = new System.Windows.Forms.Button();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.ShaperBox = new System.Windows.Forms.RichTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.panelStart = new System.Windows.Forms.Panel();
            this.ShaperButton = new System.Windows.Forms.Button();
            this.CompButton = new System.Windows.Forms.Button();
            this.TimerShaper = new System.Windows.Forms.Timer(this.components);
            this.timerError = new System.Windows.Forms.Timer(this.components);
            this.timerOpen = new System.Windows.Forms.Timer(this.components);
            this.timerEnabled = new System.Windows.Forms.Timer(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.pStateFCh.SuspendLayout();
            this.pIndicateRCh.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbReadHTRD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbWriteHTRD)).BeginInit();
            this.panel5.SuspendLayout();
            this.TabControl.SuspendLayout();
            this.Comp.SuspendLayout();
            this.ErrorPanel.SuspendLayout();
            this.pButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Duration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Power)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Error)).BeginInit();
            this.CodeName.SuspendLayout();
            this.OtherCode.SuspendLayout();
            this.PPRCHcode.SuspendLayout();
            this.FileFreqCode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EPO)).BeginInit();
            this.PollCode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxStopFreq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxStartFreq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxStepAutoReq)).BeginInit();
            this.MainPanel.SuspendLayout();
            this.Shaper.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.panelStart.SuspendLayout();
            this.SuspendLayout();
            // 
            // pStateFCh
            // 
            this.pStateFCh.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pStateFCh.BackColor = System.Drawing.Color.Gainsboro;
            this.pStateFCh.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pStateFCh.Controls.Add(this.LangBut);
            this.pStateFCh.Controls.Add(this.SendSpecialCommand);
            this.pStateFCh.Controls.Add(this.ComSpeed);
            this.pStateFCh.Controls.Add(this.UpDate);
            this.pStateFCh.Controls.Add(this.ComPortName);
            this.pStateFCh.Controls.Add(this.pIndicateRCh);
            this.pStateFCh.Controls.Add(this.panel5);
            this.pStateFCh.Location = new System.Drawing.Point(-1, 385);
            this.pStateFCh.Name = "pStateFCh";
            this.pStateFCh.Size = new System.Drawing.Size(392, 28);
            this.pStateFCh.TabIndex = 115;
            this.pStateFCh.Visible = false;
            this.pStateFCh.Paint += new System.Windows.Forms.PaintEventHandler(this.pStateFCh_Paint);
            // 
            // LangBut
            // 
            this.LangBut.BackColor = System.Drawing.SystemColors.Control;
            this.LangBut.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.LangBut.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.LangBut.FlatAppearance.BorderSize = 0;
            this.LangBut.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LangBut.Location = new System.Drawing.Point(355, 0);
            this.LangBut.Name = "LangBut";
            this.LangBut.Size = new System.Drawing.Size(34, 22);
            this.LangBut.TabIndex = 92;
            this.LangBut.Text = "Ru";
            this.toolTip1.SetToolTip(this.LangBut, "Language");
            this.LangBut.UseVisualStyleBackColor = false;
            this.LangBut.Click += new System.EventHandler(this.LangBut_Click);
            // 
            // SendSpecialCommand
            // 
            this.SendSpecialCommand.BackColor = System.Drawing.Color.LightGray;
            this.SendSpecialCommand.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.SendSpecialCommand.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.SendSpecialCommand.FlatAppearance.BorderSize = 0;
            this.SendSpecialCommand.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SendSpecialCommand.Location = new System.Drawing.Point(391, 0);
            this.SendSpecialCommand.Name = "SendSpecialCommand";
            this.SendSpecialCommand.Size = new System.Drawing.Size(22, 22);
            this.SendSpecialCommand.TabIndex = 91;
            this.SendSpecialCommand.UseVisualStyleBackColor = false;
            this.SendSpecialCommand.Click += new System.EventHandler(this.SendSpecialCommand_Click);
            // 
            // ComSpeed
            // 
            this.ComSpeed.FormattingEnabled = true;
            this.ComSpeed.Items.AddRange(new object[] {
            "1200",
            "4800",
            "9600",
            "19200",
            "38400",
            "57600",
            "115200"});
            this.ComSpeed.Location = new System.Drawing.Point(119, 0);
            this.ComSpeed.Name = "ComSpeed";
            this.ComSpeed.Size = new System.Drawing.Size(104, 21);
            this.ComSpeed.TabIndex = 90;
            this.ComSpeed.Text = "Speed";
            this.ComSpeed.SelectedIndexChanged += new System.EventHandler(this.ComSpeed_SelectedIndexChanged);
            // 
            // UpDate
            // 
            this.UpDate.BackgroundImage = global::Heterodyne.Properties.Resources.Update;
            this.UpDate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.UpDate.Location = new System.Drawing.Point(2, 1);
            this.UpDate.Name = "UpDate";
            this.UpDate.Size = new System.Drawing.Size(28, 22);
            this.UpDate.TabIndex = 5;
            this.UpDate.UseVisualStyleBackColor = true;
            this.UpDate.Click += new System.EventHandler(this.UpDate_Click);
            // 
            // ComPortName
            // 
            this.ComPortName.FormattingEnabled = true;
            this.ComPortName.Location = new System.Drawing.Point(32, 0);
            this.ComPortName.Name = "ComPortName";
            this.ComPortName.Size = new System.Drawing.Size(82, 21);
            this.ComPortName.TabIndex = 89;
            this.ComPortName.Text = "ComName";
            this.ComPortName.SelectedIndexChanged += new System.EventHandler(this.ComPortName_SelectedIndexChanged);
            // 
            // pIndicateRCh
            // 
            this.pIndicateRCh.Controls.Add(this.pbReadHTRD);
            this.pIndicateRCh.Controls.Add(this.pbWriteHTRD);
            this.pIndicateRCh.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pIndicateRCh.Location = new System.Drawing.Point(310, 0);
            this.pIndicateRCh.Name = "pIndicateRCh";
            this.pIndicateRCh.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.pIndicateRCh.Size = new System.Drawing.Size(44, 22);
            this.pIndicateRCh.TabIndex = 88;
            // 
            // pbReadHTRD
            // 
            this.pbReadHTRD.BackColor = System.Drawing.Color.Transparent;
            this.pbReadHTRD.Location = new System.Drawing.Point(24, 3);
            this.pbReadHTRD.Name = "pbReadHTRD";
            this.pbReadHTRD.Size = new System.Drawing.Size(15, 15);
            this.pbReadHTRD.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbReadHTRD.TabIndex = 24;
            this.pbReadHTRD.TabStop = false;
            // 
            // pbWriteHTRD
            // 
            this.pbWriteHTRD.BackColor = System.Drawing.Color.Transparent;
            this.pbWriteHTRD.Location = new System.Drawing.Point(3, 3);
            this.pbWriteHTRD.Name = "pbWriteHTRD";
            this.pbWriteHTRD.Size = new System.Drawing.Size(15, 15);
            this.pbWriteHTRD.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbWriteHTRD.TabIndex = 23;
            this.pbWriteHTRD.TabStop = false;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.lReceiverChannel);
            this.panel5.Controls.Add(this.bChannelHTRD);
            this.panel5.Location = new System.Drawing.Point(229, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(82, 22);
            this.panel5.TabIndex = 87;
            // 
            // lReceiverChannel
            // 
            this.lReceiverChannel.AutoSize = true;
            this.lReceiverChannel.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lReceiverChannel.Location = new System.Drawing.Point(28, 3);
            this.lReceiverChannel.Name = "lReceiverChannel";
            this.lReceiverChannel.Size = new System.Drawing.Size(41, 15);
            this.lReceiverChannel.TabIndex = 84;
            this.lReceiverChannel.Text = "HTRD";
            // 
            // bChannelHTRD
            // 
            this.bChannelHTRD.BackColor = System.Drawing.Color.Red;
            this.bChannelHTRD.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bChannelHTRD.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.bChannelHTRD.FlatAppearance.BorderSize = 0;
            this.bChannelHTRD.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bChannelHTRD.Location = new System.Drawing.Point(0, -1);
            this.bChannelHTRD.Name = "bChannelHTRD";
            this.bChannelHTRD.Size = new System.Drawing.Size(22, 22);
            this.bChannelHTRD.TabIndex = 83;
            this.bChannelHTRD.UseVisualStyleBackColor = false;
            this.bChannelHTRD.Click += new System.EventHandler(this.bReceiverChannel_Click);
            // 
            // tmrAutoSendALX
            // 
            this.tmrAutoSendALX.Tick += new System.EventHandler(this.tmrAutoSendALX_Tick);
            // 
            // TabControl
            // 
            this.TabControl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TabControl.Controls.Add(this.Comp);
            this.TabControl.Controls.Add(this.Shaper);
            this.TabControl.Location = new System.Drawing.Point(2, 1);
            this.TabControl.Name = "TabControl";
            this.TabControl.SelectedIndex = 0;
            this.TabControl.Size = new System.Drawing.Size(387, 385);
            this.TabControl.TabIndex = 116;
            this.TabControl.Visible = false;
            // 
            // Comp
            // 
            this.Comp.Controls.Add(this.AllBytesBox);
            this.Comp.Controls.Add(this.ErrorPanel);
            this.Comp.Controls.Add(this.ClearButCom);
            this.Comp.Controls.Add(this.pButton);
            this.Comp.Controls.Add(this.rtbLog);
            this.Comp.Controls.Add(this.MainPanel);
            this.Comp.Location = new System.Drawing.Point(4, 22);
            this.Comp.Name = "Comp";
            this.Comp.Padding = new System.Windows.Forms.Padding(3);
            this.Comp.Size = new System.Drawing.Size(379, 359);
            this.Comp.TabIndex = 0;
            this.Comp.Text = "Компьютер";
            this.Comp.UseVisualStyleBackColor = true;
            this.Comp.Click += new System.EventHandler(this.Comp_Click);
            // 
            // AllBytesBox
            // 
            this.AllBytesBox.AutoSize = true;
            this.AllBytesBox.Location = new System.Drawing.Point(216, 84);
            this.AllBytesBox.Name = "AllBytesBox";
            this.AllBytesBox.Size = new System.Drawing.Size(79, 17);
            this.AllBytesBox.TabIndex = 123;
            this.AllBytesBox.Text = "Все байты";
            this.AllBytesBox.UseVisualStyleBackColor = true;
            // 
            // ErrorPanel
            // 
            this.ErrorPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ErrorPanel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ErrorPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ErrorPanel.Controls.Add(this.ErrorLabel);
            this.ErrorPanel.Location = new System.Drawing.Point(0, 334);
            this.ErrorPanel.Name = "ErrorPanel";
            this.ErrorPanel.Size = new System.Drawing.Size(379, 22);
            this.ErrorPanel.TabIndex = 122;
            // 
            // ErrorLabel
            // 
            this.ErrorLabel.AutoSize = true;
            this.ErrorLabel.ForeColor = System.Drawing.Color.Maroon;
            this.ErrorLabel.Location = new System.Drawing.Point(3, 3);
            this.ErrorLabel.Name = "ErrorLabel";
            this.ErrorLabel.Size = new System.Drawing.Size(65, 13);
            this.ErrorLabel.TabIndex = 120;
            this.ErrorLabel.Text = "Сообщение";
            // 
            // ClearButCom
            // 
            this.ClearButCom.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ClearButCom.Location = new System.Drawing.Point(212, 303);
            this.ClearButCom.Name = "ClearButCom";
            this.ClearButCom.Size = new System.Drawing.Size(161, 23);
            this.ClearButCom.TabIndex = 121;
            this.ClearButCom.Text = "Очистить";
            this.ClearButCom.UseVisualStyleBackColor = true;
            this.ClearButCom.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // pButton
            // 
            this.pButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pButton.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pButton.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pButton.Controls.Add(this.DurationLab);
            this.pButton.Controls.Add(this.Duration);
            this.pButton.Controls.Add(this.ErrorLab);
            this.pButton.Controls.Add(this.PowerLab);
            this.pButton.Controls.Add(this.Power);
            this.pButton.Controls.Add(this.Error);
            this.pButton.Controls.Add(this.OffButton);
            this.pButton.Controls.Add(this.OnButton);
            this.pButton.Controls.Add(this.CodeName);
            this.pButton.Controls.Add(this.bSendALX);
            this.pButton.Location = new System.Drawing.Point(0, 0);
            this.pButton.Name = "pButton";
            this.pButton.Size = new System.Drawing.Size(209, 336);
            this.pButton.TabIndex = 119;
            // 
            // DurationLab
            // 
            this.DurationLab.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DurationLab.Location = new System.Drawing.Point(3, 266);
            this.DurationLab.Name = "DurationLab";
            this.DurationLab.Size = new System.Drawing.Size(84, 29);
            this.DurationLab.TabIndex = 25;
            this.DurationLab.Text = "Длительность излучения, сек";
            // 
            // Duration
            // 
            this.Duration.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Duration.Location = new System.Drawing.Point(122, 272);
            this.Duration.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.Duration.Name = "Duration";
            this.Duration.Size = new System.Drawing.Size(58, 20);
            this.Duration.TabIndex = 24;
            // 
            // ErrorLab
            // 
            this.ErrorLab.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ErrorLab.AutoSize = true;
            this.ErrorLab.Location = new System.Drawing.Point(159, 5);
            this.ErrorLab.Name = "ErrorLab";
            this.ErrorLab.Size = new System.Drawing.Size(47, 13);
            this.ErrorLab.TabIndex = 27;
            this.ErrorLab.Text = "Ошибка";
            // 
            // PowerLab
            // 
            this.PowerLab.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.PowerLab.AutoSize = true;
            this.PowerLab.Location = new System.Drawing.Point(4, 5);
            this.PowerLab.Name = "PowerLab";
            this.PowerLab.Size = new System.Drawing.Size(60, 13);
            this.PowerLab.TabIndex = 26;
            this.PowerLab.Text = "Мощность";
            // 
            // Power
            // 
            this.Power.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Power.BackColor = System.Drawing.Color.Transparent;
            this.Power.Location = new System.Drawing.Point(7, 25);
            this.Power.Name = "Power";
            this.Power.Size = new System.Drawing.Size(30, 32);
            this.Power.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.Power.TabIndex = 25;
            this.Power.TabStop = false;
            // 
            // Error
            // 
            this.Error.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Error.BackColor = System.Drawing.Color.Transparent;
            this.Error.Location = new System.Drawing.Point(167, 25);
            this.Error.Name = "Error";
            this.Error.Size = new System.Drawing.Size(30, 32);
            this.Error.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.Error.TabIndex = 24;
            this.Error.TabStop = false;
            // 
            // OffButton
            // 
            this.OffButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.OffButton.Location = new System.Drawing.Point(122, 303);
            this.OffButton.Name = "OffButton";
            this.OffButton.Size = new System.Drawing.Size(75, 26);
            this.OffButton.TabIndex = 8;
            this.OffButton.Text = "Выключить";
            this.OffButton.UseVisualStyleBackColor = true;
            this.OffButton.Click += new System.EventHandler(this.OffButton_Click);
            // 
            // OnButton
            // 
            this.OnButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.OnButton.Location = new System.Drawing.Point(3, 303);
            this.OnButton.Name = "OnButton";
            this.OnButton.Size = new System.Drawing.Size(75, 26);
            this.OnButton.TabIndex = 7;
            this.OnButton.Text = "Включить";
            this.OnButton.UseVisualStyleBackColor = true;
            this.OnButton.Click += new System.EventHandler(this.OnButton_Click);
            // 
            // CodeName
            // 
            this.CodeName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CodeName.Controls.Add(this.OtherCode);
            this.CodeName.Controls.Add(this.PPRCHcode);
            this.CodeName.Controls.Add(this.FileFreqCode);
            this.CodeName.Controls.Add(this.PollCode);
            this.CodeName.Location = new System.Drawing.Point(3, 63);
            this.CodeName.Name = "CodeName";
            this.CodeName.SelectedIndex = 0;
            this.CodeName.Size = new System.Drawing.Size(200, 197);
            this.CodeName.TabIndex = 6;
            this.CodeName.SelectedIndexChanged += new System.EventHandler(this.CodeName_SelectedIndexChanged);
            // 
            // OtherCode
            // 
            this.OtherCode.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.OtherCode.Controls.Add(this.TypeFreqBox);
            this.OtherCode.Controls.Add(this.FreqLabOther);
            this.OtherCode.Controls.Add(this.StartFreq);
            this.OtherCode.Controls.Add(this.Plus);
            this.OtherCode.Controls.Add(this.StepBox);
            this.OtherCode.Controls.Add(this.Minus);
            this.OtherCode.Controls.Add(this.StepLabOther);
            this.OtherCode.Location = new System.Drawing.Point(4, 22);
            this.OtherCode.Name = "OtherCode";
            this.OtherCode.Padding = new System.Windows.Forms.Padding(3);
            this.OtherCode.Size = new System.Drawing.Size(192, 171);
            this.OtherCode.TabIndex = 0;
            this.OtherCode.Text = "Другие";
            this.OtherCode.UseVisualStyleBackColor = true;
            // 
            // TypeFreqBox
            // 
            this.TypeFreqBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TypeFreqBox.CheckOnClick = true;
            this.TypeFreqBox.FormattingEnabled = true;
            this.TypeFreqBox.Items.AddRange(new object[] {
            "ФРЧ",
            "АМ",
            "ФМ",
            "ЧМ"});
            this.TypeFreqBox.Location = new System.Drawing.Point(57, 100);
            this.TypeFreqBox.Name = "TypeFreqBox";
            this.TypeFreqBox.Size = new System.Drawing.Size(83, 30);
            this.TypeFreqBox.TabIndex = 5;
            this.TypeFreqBox.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListBox1_ItemCheck);
            this.TypeFreqBox.SelectedIndexChanged += new System.EventHandler(this.checkedListBox1_SelectedIndexChanged);
            // 
            // FreqLabOther
            // 
            this.FreqLabOther.AutoSize = true;
            this.FreqLabOther.Location = new System.Drawing.Point(31, 51);
            this.FreqLabOther.Name = "FreqLabOther";
            this.FreqLabOther.Size = new System.Drawing.Size(131, 13);
            this.FreqLabOther.TabIndex = 9;
            this.FreqLabOther.Text = "Начальная частота, MГц";
            // 
            // StartFreq
            // 
            this.StartFreq.Location = new System.Drawing.Point(55, 71);
            this.StartFreq.Name = "StartFreq";
            this.StartFreq.Size = new System.Drawing.Size(74, 20);
            this.StartFreq.TabIndex = 10;
            this.StartFreq.TextChanged += new System.EventHandler(this.StartFreq_TextChanged);
            this.StartFreq.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.StartFreq_KeyPress);
            // 
            // Plus
            // 
            this.Plus.Location = new System.Drawing.Point(137, 67);
            this.Plus.Name = "Plus";
            this.Plus.Size = new System.Drawing.Size(44, 24);
            this.Plus.TabIndex = 13;
            this.Plus.Text = ">>";
            this.Plus.UseVisualStyleBackColor = true;
            this.Plus.Click += new System.EventHandler(this.Plus_Click);
            // 
            // StepBox
            // 
            this.StepBox.Location = new System.Drawing.Point(55, 19);
            this.StepBox.Name = "StepBox";
            this.StepBox.Size = new System.Drawing.Size(74, 20);
            this.StepBox.TabIndex = 12;
            this.StepBox.TextChanged += new System.EventHandler(this.StepBox_TextChanged);
            this.StepBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.StepBox_KeyPress);
            // 
            // Minus
            // 
            this.Minus.Location = new System.Drawing.Point(7, 67);
            this.Minus.Name = "Minus";
            this.Minus.Size = new System.Drawing.Size(44, 24);
            this.Minus.TabIndex = 14;
            this.Minus.Text = "<<";
            this.Minus.UseVisualStyleBackColor = true;
            this.Minus.Click += new System.EventHandler(this.Minus_Click);
            // 
            // StepLabOther
            // 
            this.StepLabOther.AutoSize = true;
            this.StepLabOther.Location = new System.Drawing.Point(31, 3);
            this.StepLabOther.Name = "StepLabOther";
            this.StepLabOther.Size = new System.Drawing.Size(122, 13);
            this.StepLabOther.TabIndex = 11;
            this.StepLabOther.Text = "Шаг перестройки, MГц";
            // 
            // PPRCHcode
            // 
            this.PPRCHcode.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PPRCHcode.Controls.Add(this.StepStartFreq);
            this.PPRCHcode.Controls.Add(this.StepLabPPRCH);
            this.PPRCHcode.Controls.Add(this.FreqLabPPRCH);
            this.PPRCHcode.Controls.Add(this.StartFreqPPRCH);
            this.PPRCHcode.Controls.Add(this.PlusPPRCH);
            this.PPRCHcode.Controls.Add(this.MinusPPRCH);
            this.PPRCHcode.Controls.Add(this.NumFreq);
            this.PPRCHcode.Controls.Add(this.SaltusBox);
            this.PPRCHcode.Controls.Add(this.FregOfJampLab);
            this.PPRCHcode.Controls.Add(this.StepPPRCHBox);
            this.PPRCHcode.Controls.Add(this.StepTuningLab);
            this.PPRCHcode.Controls.Add(this.NumFreqLab);
            this.PPRCHcode.Location = new System.Drawing.Point(4, 22);
            this.PPRCHcode.Name = "PPRCHcode";
            this.PPRCHcode.Padding = new System.Windows.Forms.Padding(3);
            this.PPRCHcode.Size = new System.Drawing.Size(192, 171);
            this.PPRCHcode.TabIndex = 1;
            this.PPRCHcode.Text = "ППРЧ";
            this.PPRCHcode.UseVisualStyleBackColor = true;
            // 
            // StepStartFreq
            // 
            this.StepStartFreq.Location = new System.Drawing.Point(55, 19);
            this.StepStartFreq.Name = "StepStartFreq";
            this.StepStartFreq.Size = new System.Drawing.Size(74, 20);
            this.StepStartFreq.TabIndex = 20;
            this.StepStartFreq.TextChanged += new System.EventHandler(this.StepStartFreq_TextChanged);
            // 
            // StepLabPPRCH
            // 
            this.StepLabPPRCH.AutoSize = true;
            this.StepLabPPRCH.Location = new System.Drawing.Point(31, 3);
            this.StepLabPPRCH.Name = "StepLabPPRCH";
            this.StepLabPPRCH.Size = new System.Drawing.Size(122, 13);
            this.StepLabPPRCH.TabIndex = 19;
            this.StepLabPPRCH.Text = "Шаг перестройки, MГц";
            // 
            // FreqLabPPRCH
            // 
            this.FreqLabPPRCH.AutoSize = true;
            this.FreqLabPPRCH.Location = new System.Drawing.Point(30, 43);
            this.FreqLabPPRCH.Name = "FreqLabPPRCH";
            this.FreqLabPPRCH.Size = new System.Drawing.Size(131, 13);
            this.FreqLabPPRCH.TabIndex = 15;
            this.FreqLabPPRCH.Text = "Начальная частота, MГц";
            // 
            // StartFreqPPRCH
            // 
            this.StartFreqPPRCH.Location = new System.Drawing.Point(55, 62);
            this.StartFreqPPRCH.Name = "StartFreqPPRCH";
            this.StartFreqPPRCH.Size = new System.Drawing.Size(74, 20);
            this.StartFreqPPRCH.TabIndex = 16;
            this.StartFreqPPRCH.TextChanged += new System.EventHandler(this.StartFreqPPRCH_TextChanged);
            // 
            // PlusPPRCH
            // 
            this.PlusPPRCH.Location = new System.Drawing.Point(143, 59);
            this.PlusPPRCH.Name = "PlusPPRCH";
            this.PlusPPRCH.Size = new System.Drawing.Size(44, 24);
            this.PlusPPRCH.TabIndex = 17;
            this.PlusPPRCH.Text = ">>";
            this.PlusPPRCH.UseVisualStyleBackColor = true;
            this.PlusPPRCH.Click += new System.EventHandler(this.PlusPPRCH_Click);
            // 
            // MinusPPRCH
            // 
            this.MinusPPRCH.Location = new System.Drawing.Point(1, 59);
            this.MinusPPRCH.Name = "MinusPPRCH";
            this.MinusPPRCH.Size = new System.Drawing.Size(44, 24);
            this.MinusPPRCH.TabIndex = 18;
            this.MinusPPRCH.Text = "<<";
            this.MinusPPRCH.UseVisualStyleBackColor = true;
            this.MinusPPRCH.Click += new System.EventHandler(this.MinusPPRCH_Click);
            // 
            // NumFreq
            // 
            this.NumFreq.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.NumFreq.FormattingEnabled = true;
            this.NumFreq.Items.AddRange(new object[] {
            "2",
            "4",
            "8",
            "16",
            "32",
            "64",
            "121"});
            this.NumFreq.Location = new System.Drawing.Point(122, 88);
            this.NumFreq.Name = "NumFreq";
            this.NumFreq.Size = new System.Drawing.Size(65, 21);
            this.NumFreq.TabIndex = 6;
            // 
            // SaltusBox
            // 
            this.SaltusBox.Location = new System.Drawing.Point(122, 140);
            this.SaltusBox.Name = "SaltusBox";
            this.SaltusBox.Size = new System.Drawing.Size(65, 20);
            this.SaltusBox.TabIndex = 5;
            this.SaltusBox.TextChanged += new System.EventHandler(this.SaltusBox_TextChanged);
            this.SaltusBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SaltusBox_KeyPress);
            this.SaltusBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.SaltusBox_KeyUp);
            // 
            // FregOfJampLab
            // 
            this.FregOfJampLab.AutoSize = true;
            this.FregOfJampLab.Location = new System.Drawing.Point(3, 140);
            this.FregOfJampLab.Name = "FregOfJampLab";
            this.FregOfJampLab.Size = new System.Drawing.Size(96, 26);
            this.FregOfJampLab.TabIndex = 4;
            this.FregOfJampLab.Text = "Частота скачков \r\n(ск/с)";
            // 
            // StepPPRCHBox
            // 
            this.StepPPRCHBox.Location = new System.Drawing.Point(122, 115);
            this.StepPPRCHBox.Name = "StepPPRCHBox";
            this.StepPPRCHBox.Size = new System.Drawing.Size(65, 20);
            this.StepPPRCHBox.TabIndex = 3;
            this.StepPPRCHBox.TextChanged += new System.EventHandler(this.StepPPRCHBox_TextChanged);
            this.StepPPRCHBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.StepPPRCHBox_KeyPress);
            this.StepPPRCHBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.StepPPRCHBox_KeyUp);
            // 
            // StepTuningLab
            // 
            this.StepTuningLab.AutoSize = true;
            this.StepTuningLab.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.StepTuningLab.Location = new System.Drawing.Point(3, 111);
            this.StepTuningLab.MaximumSize = new System.Drawing.Size(95, 26);
            this.StepTuningLab.MinimumSize = new System.Drawing.Size(95, 26);
            this.StepTuningLab.Name = "StepTuningLab";
            this.StepTuningLab.Size = new System.Drawing.Size(95, 26);
            this.StepTuningLab.TabIndex = 2;
            this.StepTuningLab.Text = "Шаг перестройки\r\nпо частоте, кГц\r\n";
            // 
            // NumFreqLab
            // 
            this.NumFreqLab.AutoSize = true;
            this.NumFreqLab.Location = new System.Drawing.Point(3, 89);
            this.NumFreqLab.Name = "NumFreqLab";
            this.NumFreqLab.Size = new System.Drawing.Size(102, 13);
            this.NumFreqLab.TabIndex = 0;
            this.NumFreqLab.Text = "Количество частот";
            // 
            // FileFreqCode
            // 
            this.FileFreqCode.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.FileFreqCode.Controls.Add(this.CheckBoxShift);
            this.FileFreqCode.Controls.Add(this.label_NumStrip);
            this.FileFreqCode.Controls.Add(this.label18);
            this.FileFreqCode.Controls.Add(this.button2);
            this.FileFreqCode.Controls.Add(this.button3);
            this.FileFreqCode.Controls.Add(this.label17);
            this.FileFreqCode.Controls.Add(this.StepFileFreq);
            this.FileFreqCode.Controls.Add(this.label14);
            this.FileFreqCode.Controls.Add(this.NuberFileFreqLabel);
            this.FileFreqCode.Controls.Add(this.EPO);
            this.FileFreqCode.Location = new System.Drawing.Point(4, 22);
            this.FileFreqCode.Name = "FileFreqCode";
            this.FileFreqCode.Padding = new System.Windows.Forms.Padding(3);
            this.FileFreqCode.Size = new System.Drawing.Size(192, 171);
            this.FileFreqCode.TabIndex = 2;
            this.FileFreqCode.Text = "Сетка частот";
            this.FileFreqCode.UseVisualStyleBackColor = true;
            this.FileFreqCode.Click += new System.EventHandler(this.FileFreq_Click);
            // 
            // CheckBoxShift
            // 
            this.CheckBoxShift.AccessibleRole = System.Windows.Forms.AccessibleRole.MenuBar;
            this.CheckBoxShift.AutoSize = true;
            this.CheckBoxShift.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CheckBoxShift.Location = new System.Drawing.Point(4, 45);
            this.CheckBoxShift.Name = "CheckBoxShift";
            this.CheckBoxShift.Size = new System.Drawing.Size(145, 17);
            this.CheckBoxShift.TabIndex = 24;
            this.CheckBoxShift.Text = "Добавление смещения";
            this.CheckBoxShift.UseVisualStyleBackColor = true;
            // 
            // label_NumStrip
            // 
            this.label_NumStrip.AutoSize = true;
            this.label_NumStrip.Location = new System.Drawing.Point(108, 84);
            this.label_NumStrip.Name = "label_NumStrip";
            this.label_NumStrip.Size = new System.Drawing.Size(58, 13);
            this.label_NumStrip.TabIndex = 23;
            this.label_NumStrip.Text = "Диапазон";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(1, 82);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(85, 13);
            this.label18.TabIndex = 19;
            this.label18.Text = "Диапазон, MГц";
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Location = new System.Drawing.Point(167, 78);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(18, 24);
            this.button2.TabIndex = 21;
            this.button2.Text = ">>";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.Location = new System.Drawing.Point(88, 78);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(18, 24);
            this.button3.TabIndex = 22;
            this.button3.Text = "<<";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(1, 10);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(64, 13);
            this.label17.TabIndex = 8;
            this.label17.Text = "№ Полосы:";
            // 
            // StepFileFreq
            // 
            this.StepFileFreq.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.StepFileFreq.FormattingEnabled = true;
            this.StepFileFreq.Items.AddRange(new object[] {
            "1",
            "2",
            "5",
            "15"});
            this.StepFileFreq.Location = new System.Drawing.Point(101, 121);
            this.StepFileFreq.Name = "StepFileFreq";
            this.StepFileFreq.Size = new System.Drawing.Size(65, 21);
            this.StepFileFreq.TabIndex = 7;
            this.StepFileFreq.SelectedIndexChanged += new System.EventHandler(this.StepFileFreq_SelectedIndexChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(1, 124);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(54, 13);
            this.label14.TabIndex = 3;
            this.label14.Text = "Шаг, MГц";
            // 
            // NuberFileFreqLabel
            // 
            this.NuberFileFreqLabel.AutoSize = true;
            this.NuberFileFreqLabel.Location = new System.Drawing.Point(30, 151);
            this.NuberFileFreqLabel.Name = "NuberFileFreqLabel";
            this.NuberFileFreqLabel.Size = new System.Drawing.Size(102, 13);
            this.NuberFileFreqLabel.TabIndex = 1;
            this.NuberFileFreqLabel.Text = "Количество частот";
            // 
            // EPO
            // 
            this.EPO.Location = new System.Drawing.Point(110, 6);
            this.EPO.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.EPO.Name = "EPO";
            this.EPO.Size = new System.Drawing.Size(58, 20);
            this.EPO.TabIndex = 0;
            this.EPO.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.EPO.ValueChanged += new System.EventHandler(this.EPO_ValueChanged);
            // 
            // PollCode
            // 
            this.PollCode.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.PollCode.Controls.Add(this.LabCurrentFreq);
            this.PollCode.Controls.Add(this.NumBoxStopFreq);
            this.PollCode.Controls.Add(this.label3);
            this.PollCode.Controls.Add(this.NumBoxStartFreq);
            this.PollCode.Controls.Add(this.NumBoxStepAutoReq);
            this.PollCode.Controls.Add(this.label1);
            this.PollCode.Controls.Add(this.LabStepAutoReq);
            this.PollCode.Location = new System.Drawing.Point(4, 22);
            this.PollCode.Name = "PollCode";
            this.PollCode.Padding = new System.Windows.Forms.Padding(3);
            this.PollCode.Size = new System.Drawing.Size(192, 171);
            this.PollCode.TabIndex = 3;
            this.PollCode.Text = "Опроос";
            this.PollCode.UseVisualStyleBackColor = true;
            // 
            // LabCurrentFreq
            // 
            this.LabCurrentFreq.AutoSize = true;
            this.LabCurrentFreq.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabCurrentFreq.Location = new System.Drawing.Point(6, 149);
            this.LabCurrentFreq.Name = "LabCurrentFreq";
            this.LabCurrentFreq.Size = new System.Drawing.Size(103, 15);
            this.LabCurrentFreq.TabIndex = 25;
            this.LabCurrentFreq.Text = "Текущая частота";
            this.LabCurrentFreq.Visible = false;
            // 
            // NumBoxStopFreq
            // 
            this.NumBoxStopFreq.Location = new System.Drawing.Point(124, 67);
            this.NumBoxStopFreq.Maximum = new decimal(new int[] {
            6000,
            0,
            0,
            0});
            this.NumBoxStopFreq.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.NumBoxStopFreq.Name = "NumBoxStopFreq";
            this.NumBoxStopFreq.Size = new System.Drawing.Size(58, 20);
            this.NumBoxStopFreq.TabIndex = 24;
            this.NumBoxStopFreq.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 13);
            this.label3.TabIndex = 23;
            this.label3.Text = "Частота СТОП, MГц";
            // 
            // NumBoxStartFreq
            // 
            this.NumBoxStartFreq.Location = new System.Drawing.Point(124, 30);
            this.NumBoxStartFreq.Maximum = new decimal(new int[] {
            6000,
            0,
            0,
            0});
            this.NumBoxStartFreq.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.NumBoxStartFreq.Name = "NumBoxStartFreq";
            this.NumBoxStartFreq.Size = new System.Drawing.Size(58, 20);
            this.NumBoxStartFreq.TabIndex = 22;
            this.NumBoxStartFreq.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // NumBoxStepAutoReq
            // 
            this.NumBoxStepAutoReq.Location = new System.Drawing.Point(124, 104);
            this.NumBoxStepAutoReq.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.NumBoxStepAutoReq.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumBoxStepAutoReq.Name = "NumBoxStepAutoReq";
            this.NumBoxStepAutoReq.Size = new System.Drawing.Size(58, 20);
            this.NumBoxStepAutoReq.TabIndex = 21;
            this.NumBoxStepAutoReq.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Частота СТАРТ, MГц";
            // 
            // LabStepAutoReq
            // 
            this.LabStepAutoReq.AutoSize = true;
            this.LabStepAutoReq.Location = new System.Drawing.Point(6, 111);
            this.LabStepAutoReq.Name = "LabStepAutoReq";
            this.LabStepAutoReq.Size = new System.Drawing.Size(54, 13);
            this.LabStepAutoReq.TabIndex = 17;
            this.LabStepAutoReq.Text = "Шаг, MГц";
            // 
            // bSendALX
            // 
            this.bSendALX.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.bSendALX.Location = new System.Drawing.Point(42, 25);
            this.bSendALX.Name = "bSendALX";
            this.bSendALX.Size = new System.Drawing.Size(119, 32);
            this.bSendALX.TabIndex = 0;
            this.bSendALX.Text = "Инициализация";
            this.bSendALX.UseVisualStyleBackColor = true;
            this.bSendALX.Click += new System.EventHandler(this.bSendALX_Click_1);
            // 
            // rtbLog
            // 
            this.rtbLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbLog.BackColor = System.Drawing.Color.Tan;
            this.rtbLog.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rtbLog.Location = new System.Drawing.Point(210, 107);
            this.rtbLog.Name = "rtbLog";
            this.rtbLog.ReadOnly = true;
            this.rtbLog.Size = new System.Drawing.Size(166, 193);
            this.rtbLog.TabIndex = 120;
            this.rtbLog.Text = "";
            // 
            // MainPanel
            // 
            this.MainPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MainPanel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.MainPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.MainPanel.Controls.Add(this.Chargelabel);
            this.MainPanel.Controls.Add(this.Humididylabel);
            this.MainPanel.Controls.Add(this.Templabel);
            this.MainPanel.Location = new System.Drawing.Point(210, 3);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(166, 76);
            this.MainPanel.TabIndex = 5;
            // 
            // Chargelabel
            // 
            this.Chargelabel.AutoSize = true;
            this.Chargelabel.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Chargelabel.Location = new System.Drawing.Point(3, 47);
            this.Chargelabel.Name = "Chargelabel";
            this.Chargelabel.Size = new System.Drawing.Size(62, 13);
            this.Chargelabel.TabIndex = 120;
            this.Chargelabel.Text = "Заряд АКБ";
            // 
            // Humididylabel
            // 
            this.Humididylabel.AutoSize = true;
            this.Humididylabel.Location = new System.Drawing.Point(3, 25);
            this.Humididylabel.Name = "Humididylabel";
            this.Humididylabel.Size = new System.Drawing.Size(63, 13);
            this.Humididylabel.TabIndex = 119;
            this.Humididylabel.Text = "Влажность";
            // 
            // Templabel
            // 
            this.Templabel.AutoSize = true;
            this.Templabel.Location = new System.Drawing.Point(3, 1);
            this.Templabel.Name = "Templabel";
            this.Templabel.Size = new System.Drawing.Size(74, 13);
            this.Templabel.TabIndex = 3;
            this.Templabel.Text = "Температура";
            // 
            // Shaper
            // 
            this.Shaper.Controls.Add(this.checkBox2);
            this.Shaper.Controls.Add(this.ErroreListBox);
            this.Shaper.Controls.Add(this.panel2);
            this.Shaper.Controls.Add(this.ClearButShaper);
            this.Shaper.Controls.Add(this.numericUpDown1);
            this.Shaper.Controls.Add(this.ShaperBox);
            this.Shaper.Controls.Add(this.label10);
            this.Shaper.Location = new System.Drawing.Point(4, 22);
            this.Shaper.Name = "Shaper";
            this.Shaper.Padding = new System.Windows.Forms.Padding(3);
            this.Shaper.Size = new System.Drawing.Size(379, 359);
            this.Shaper.TabIndex = 1;
            this.Shaper.Text = "Формирователь";
            this.Shaper.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(257, 168);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(79, 17);
            this.checkBox2.TabIndex = 132;
            this.checkBox2.Text = "Все байты";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // ErroreListBox
            // 
            this.ErroreListBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ErroreListBox.CheckOnClick = true;
            this.ErroreListBox.FormattingEnabled = true;
            this.ErroreListBox.Items.AddRange(new object[] {
            "Нет ошибок",
            "Программно неопределенная",
            "Аппаратно неопределенная",
            "Ошибка CRC модем",
            "Ошибка CRC генератор",
            "Ошибка CRC формирователь",
            "Ошибка ввода данных",
            "Нет связи с Формирователем",
            "ПР - на ЖК при приеме с ПК (внутренние для ЖК)",
            "ПРД - на ЖК при передаче на ПК (внутренние для ЖК)"});
            this.ErroreListBox.Location = new System.Drawing.Point(10, 4);
            this.ErroreListBox.Name = "ErroreListBox";
            this.ErroreListBox.Size = new System.Drawing.Size(366, 107);
            this.ErroreListBox.TabIndex = 128;
            this.ErroreListBox.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.ErroreListBox_ItemCheck_1);
            this.ErroreListBox.SelectedIndexChanged += new System.EventHandler(this.ErroreListBox_SelectedIndexChanged);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Location = new System.Drawing.Point(9, 248);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(363, 98);
            this.panel2.TabIndex = 126;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 13);
            this.label6.TabIndex = 122;
            this.label6.Text = "Температура";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 77);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 13);
            this.label9.TabIndex = 125;
            this.label9.Text = "Сообщение";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 32);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 13);
            this.label7.TabIndex = 123;
            this.label7.Text = "Влажность";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 53);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 124;
            this.label8.Text = "Заряд";
            // 
            // ClearButShaper
            // 
            this.ClearButShaper.Location = new System.Drawing.Point(252, 212);
            this.ClearButShaper.Name = "ClearButShaper";
            this.ClearButShaper.Size = new System.Drawing.Size(84, 30);
            this.ClearButShaper.TabIndex = 127;
            this.ClearButShaper.Text = "Очистить";
            this.ClearButShaper.UseVisualStyleBackColor = true;
            this.ClearButShaper.Click += new System.EventHandler(this.button1_Click);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(143, 142);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(59, 20);
            this.numericUpDown1.TabIndex = 131;
            this.numericUpDown1.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            this.numericUpDown1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numericUpDown1_KeyPress);
            this.numericUpDown1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.numericUpDown1_KeyUp);
            // 
            // ShaperBox
            // 
            this.ShaperBox.BackColor = System.Drawing.Color.Tan;
            this.ShaperBox.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ShaperBox.Location = new System.Drawing.Point(8, 168);
            this.ShaperBox.Name = "ShaperBox";
            this.ShaperBox.ReadOnly = true;
            this.ShaperBox.Size = new System.Drawing.Size(237, 74);
            this.ShaperBox.TabIndex = 121;
            this.ShaperBox.Text = "";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 144);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(142, 13);
            this.label10.TabIndex = 130;
            this.label10.Text = "Время задержки ответа....";
            // 
            // panelStart
            // 
            this.panelStart.Controls.Add(this.ShaperButton);
            this.panelStart.Controls.Add(this.CompButton);
            this.panelStart.Location = new System.Drawing.Point(77, 145);
            this.panelStart.Name = "panelStart";
            this.panelStart.Size = new System.Drawing.Size(267, 160);
            this.panelStart.TabIndex = 118;
            // 
            // ShaperButton
            // 
            this.ShaperButton.Location = new System.Drawing.Point(136, 67);
            this.ShaperButton.Name = "ShaperButton";
            this.ShaperButton.Size = new System.Drawing.Size(108, 31);
            this.ShaperButton.TabIndex = 1;
            this.ShaperButton.Text = "Формирователь";
            this.ShaperButton.UseVisualStyleBackColor = true;
            this.ShaperButton.Click += new System.EventHandler(this.ShaperButton_Click);
            // 
            // CompButton
            // 
            this.CompButton.Location = new System.Drawing.Point(20, 65);
            this.CompButton.Name = "CompButton";
            this.CompButton.Size = new System.Drawing.Size(86, 33);
            this.CompButton.TabIndex = 0;
            this.CompButton.Text = "Компьютер";
            this.CompButton.UseVisualStyleBackColor = true;
            this.CompButton.Click += new System.EventHandler(this.CompButton_Click);
            // 
            // TimerShaper
            // 
            this.TimerShaper.Tick += new System.EventHandler(this.TimerShaper_Tick);
            // 
            // timerError
            // 
            this.timerError.Interval = 1000;
            this.timerError.Tick += new System.EventHandler(this.timerError_Tick);
            // 
            // timerOpen
            // 
            this.timerOpen.Interval = 1000;
            this.timerOpen.Tick += new System.EventHandler(this.timerOpen_Tick);
            // 
            // timerEnabled
            // 
            this.timerEnabled.Interval = 1000;
            this.timerEnabled.Tick += new System.EventHandler(this.timerEnabled_Tick);
            // 
            // FormHeter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(388, 411);
            this.Controls.Add(this.TabControl);
            this.Controls.Add(this.pStateFCh);
            this.Controls.Add(this.panelStart);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(404, 439);
            this.Name = "FormHeter";
            this.Text = "Control Heterodyne ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.pStateFCh.ResumeLayout(false);
            this.pIndicateRCh.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbReadHTRD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbWriteHTRD)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.TabControl.ResumeLayout(false);
            this.Comp.ResumeLayout(false);
            this.Comp.PerformLayout();
            this.ErrorPanel.ResumeLayout(false);
            this.ErrorPanel.PerformLayout();
            this.pButton.ResumeLayout(false);
            this.pButton.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Duration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Power)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Error)).EndInit();
            this.CodeName.ResumeLayout(false);
            this.OtherCode.ResumeLayout(false);
            this.OtherCode.PerformLayout();
            this.PPRCHcode.ResumeLayout(false);
            this.PPRCHcode.PerformLayout();
            this.FileFreqCode.ResumeLayout(false);
            this.FileFreqCode.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.EPO)).EndInit();
            this.PollCode.ResumeLayout(false);
            this.PollCode.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxStopFreq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxStartFreq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBoxStepAutoReq)).EndInit();
            this.MainPanel.ResumeLayout(false);
            this.MainPanel.PerformLayout();
            this.Shaper.ResumeLayout(false);
            this.Shaper.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.panelStart.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pStateFCh;
        private System.Windows.Forms.PictureBox pbReadHTRD;
        private System.Windows.Forms.PictureBox pbWriteHTRD;
        private System.Windows.Forms.Label lReceiverChannel;
        private System.Windows.Forms.Button bChannelHTRD;
        private System.Windows.Forms.Timer tmrAutoSendALX;
        public System.Windows.Forms.Panel pIndicateRCh;
        public System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ComboBox ComPortName;
        private System.Windows.Forms.Button UpDate;
        private System.Windows.Forms.TabControl TabControl;
        private System.Windows.Forms.TabPage Comp;
        private System.Windows.Forms.RichTextBox rtbLog;
        private System.Windows.Forms.Panel pButton;
        private System.Windows.Forms.Button bSendALX;
        private System.Windows.Forms.TabPage Shaper;
        private System.Windows.Forms.RichTextBox ShaperBox;
        private System.Windows.Forms.Panel MainPanel;
        private System.Windows.Forms.Panel panelStart;
        private System.Windows.Forms.Button ShaperButton;
        private System.Windows.Forms.Button CompButton;
        private System.Windows.Forms.TextBox StepBox;
        private System.Windows.Forms.Label StepLabOther;
        private System.Windows.Forms.Button OffButton;
        private System.Windows.Forms.Button OnButton;
        private System.Windows.Forms.TabControl CodeName;
        private System.Windows.Forms.TabPage OtherCode;
        private System.Windows.Forms.TabPage PPRCHcode;
        private System.Windows.Forms.Label NumFreqLab;
        private System.Windows.Forms.TextBox SaltusBox;
        private System.Windows.Forms.Label FregOfJampLab;
        private System.Windows.Forms.TextBox StepPPRCHBox;
        public System.Windows.Forms.Label StepTuningLab;
        private System.Windows.Forms.Label Chargelabel;
        private System.Windows.Forms.Label Humididylabel;
        private System.Windows.Forms.Label Templabel;
        private System.Windows.Forms.CheckedListBox TypeFreqBox;
        private System.Windows.Forms.Button ClearButCom;
        private System.Windows.Forms.ComboBox NumFreq;
        private System.Windows.Forms.Panel ErrorPanel;
        private System.Windows.Forms.Label ErrorLabel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button ClearButShaper;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox ComSpeed;
        private System.Windows.Forms.CheckedListBox ErroreListBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Timer TimerShaper;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Timer timerError;
        private System.Windows.Forms.Timer timerOpen;
        private System.Windows.Forms.Timer timerEnabled;
        private System.Windows.Forms.Label ErrorLab;
        private System.Windows.Forms.Label PowerLab;
        private System.Windows.Forms.PictureBox Power;
        private System.Windows.Forms.PictureBox Error;
        private System.Windows.Forms.TabPage FileFreqCode;
        private System.Windows.Forms.ComboBox StepFileFreq;
        public System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label NuberFileFreqLabel;
        private System.Windows.Forms.Button Minus;
        private System.Windows.Forms.Button Plus;
        private System.Windows.Forms.TextBox StartFreq;
        private System.Windows.Forms.Label FreqLabOther;
        private System.Windows.Forms.Label FreqLabPPRCH;
        private System.Windows.Forms.TextBox StartFreqPPRCH;
        private System.Windows.Forms.Button PlusPPRCH;
        private System.Windows.Forms.Button MinusPPRCH;
        private System.Windows.Forms.TextBox StepStartFreq;
        private System.Windows.Forms.Label StepLabPPRCH;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown EPO;
        private System.Windows.Forms.Label label_NumStrip;
        private System.Windows.Forms.Label DurationLab;
        private System.Windows.Forms.NumericUpDown Duration;
        private System.Windows.Forms.Button SendSpecialCommand;
        private System.Windows.Forms.CheckBox AllBytesBox;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Button LangBut;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TabPage PollCode;
        private System.Windows.Forms.NumericUpDown NumBoxStopFreq;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown NumBoxStartFreq;
        private System.Windows.Forms.NumericUpDown NumBoxStepAutoReq;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LabStepAutoReq;
        private System.Windows.Forms.Label LabCurrentFreq;
        private System.Windows.Forms.CheckBox CheckBoxShift;
    }
}

